EESchema Schematic File Version 2
LIBS:con-jack
LIBS:adm3101e
LIBS:microsd
LIBS:transistor-npn
LIBS:ipc-7351-transistor
LIBS:switch-misc
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:micron_ddr_512Mb
LIBS:iMX23
LIBS:sdmmc
LIBS:usbconn
LIBS:fsusb20
LIBS:r_pack2
LIBS:pasives-connectors
LIBS:EEPROM
LIBS:PWR
LIBS:m25p32
LIBS:PROpendous-cache
LIBS:w_analog
LIBS:gl850g
LIBS:srf2012
LIBS:rclamp0502b
LIBS:mcp130
LIBS:ABM8G
LIBS:usb_a
LIBS:Reset
LIBS:stm32f100vxx
LIBS:lt1117cst
LIBS:zxmhc3f381n8
LIBS:fsusb43
LIBS:usb-mini
LIBS:atsam3n0aa-au
LIBS:xbeepro
LIBS:ba50dd0whfp
LIBS:mic29301
LIBS:sp481
LIBS:lm35
LIBS:enc28j60
LIBS:m-pad-2.1
LIBS:ds3231
LIBS:5v600ma
LIBS:stamp_mini_4_layers-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 7
Title "noname.sch"
Date "11 sep 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L VDD5V #PWR030
U 1 1 51C87C98
P 7850 1800
F 0 "#PWR030" H 7850 1900 30  0001 C CNN
F 1 "VDD5V" H 7850 1900 30  0000 C CNN
F 2 "" H 7850 1800 60  0001 C CNN
F 3 "" H 7850 1800 60  0001 C CNN
	1    7850 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 1800 8150 1800
$Comp
L C CRE21
U 1 1 51C87CD4
P 8150 2050
F 0 "CRE21" H 8200 2150 50  0000 L CNN
F 1 "22uF" H 8150 1950 50  0000 L CNN
F 2 "SM1206" H 8150 2050 60  0001 C CNN
F 3 "" H 8150 2050 60  0001 C CNN
	1    8150 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 51C87CEC
P 7500 2300
F 0 "#PWR031" H 7500 2300 30  0001 C CNN
F 1 "GND" H 7500 2230 30  0001 C CNN
F 2 "" H 7500 2300 60  0001 C CNN
F 3 "" H 7500 2300 60  0001 C CNN
	1    7500 2300
	1    0    0    -1  
$EndComp
Connection ~ 7750 1800
$Comp
L C CRE2
U 1 1 52E34720
P 7750 2000
F 0 "CRE2" H 7800 2100 50  0000 L CNN
F 1 "0.1uF" H 7800 1900 50  0000 L CNN
F 2 "SM0603" H 7750 2000 60  0001 C CNN
F 3 "" H 7750 2000 60  0001 C CNN
	1    7750 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR032
U 1 1 52E34747
P 7750 2300
F 0 "#PWR032" H 7750 2300 30  0001 C CNN
F 1 "GND" H 7750 2230 30  0001 C CNN
F 2 "" H 7750 2300 60  0001 C CNN
F 3 "" H 7750 2300 60  0001 C CNN
	1    7750 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2200 7750 2300
$Comp
L 5v600ma PWS1
U 1 1 54C593DF
P 6750 2000
F 0 "PWS1" H 6850 2150 60  0000 C CNN
F 1 "5v600ma" H 6850 1950 60  0000 C CNN
F 2 "ac_dc:5v600ma" H 6750 2000 60  0001 C CNN
F 3 "" H 6750 2000 60  0000 C CNN
	1    6750 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 2200 7500 2200
Wire Wire Line
	7500 2200 7500 2300
Wire Wire Line
	8150 1800 8150 1850
Connection ~ 7850 1800
$Comp
L GND #PWR033
U 1 1 54C59A79
P 8150 2300
F 0 "#PWR033" H 8150 2300 30  0001 C CNN
F 1 "GND" H 8150 2230 30  0001 C CNN
F 2 "" H 8150 2300 60  0001 C CNN
F 3 "" H 8150 2300 60  0001 C CNN
	1    8150 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 2250 8150 2300
$EndSCHEMATC
