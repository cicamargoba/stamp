EESchema Schematic File Version 2
LIBS:stamp_mini_4_layers-rescue
LIBS:con-jack
LIBS:adm3101e
LIBS:microsd
LIBS:transistor-npn
LIBS:ipc-7351-transistor
LIBS:switch-misc
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:micron_ddr_512Mb
LIBS:iMX23
LIBS:sdmmc
LIBS:usbconn
LIBS:fsusb20
LIBS:r_pack2
LIBS:pasives-connectors
LIBS:EEPROM
LIBS:PWR
LIBS:m25p32
LIBS:PROpendous-cache
LIBS:w_analog
LIBS:gl850g
LIBS:srf2012
LIBS:rclamp0502b
LIBS:mcp130
LIBS:ABM8G
LIBS:usb_a
LIBS:Reset
LIBS:stm32f100vxx
LIBS:lt1117cst
LIBS:zxmhc3f381n8
LIBS:fsusb43
LIBS:usb-mini
LIBS:atsam3n0aa-au
LIBS:drv8835
LIBS:ftdichip
LIBS:stm32f4_lqfp100
LIBS:cstcr6m00g53z
LIBS:ESP8266
LIBS:LM2623
LIBS:lm3478
LIBS:stamp_mini_4_layers-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date "9 may 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2900 2250 4000 2250
Wire Wire Line
	2900 2050 4000 2050
Wire Wire Line
	2900 2150 4000 2150
Wire Wire Line
	2900 2350 4000 2350
Wire Wire Line
	2900 2450 4000 2450
Wire Wire Line
	2900 2700 4000 2700
Wire Wire Line
	2900 2800 4000 2800
$Sheet
S 4000 1750 1150 1250
U 5439B76D
F0 "serial_interface" 60
F1 "serial_interface.sch" 60
F2 "TCK" I L 4000 2350 60 
F3 "TDI" I L 4000 2250 60 
F4 "TDO" I L 4000 2150 60 
F5 "TMS" I L 4000 2450 60 
F6 "nTRST" I L 4000 2050 60 
F7 "nSRST" I L 4000 2550 60 
F8 "RX" I L 4000 2700 60 
F9 "TX" I L 4000 2800 60 
F10 "RST_ESP" I L 4000 1900 60 
$EndSheet
$Comp
L CONN_1 HS1
U 1 1 543C7615
P 7350 3150
F 0 "HS1" H 7430 3150 40  0000 L CNN
F 1 "CONN_1" H 7350 3205 30  0001 C CNN
F 2 "opendous:1Pin_TH_40milVia" H 7350 3150 60  0001 C CNN
F 3 "" H 7350 3150 60  0000 C CNN
	1    7350 3150
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 HS2
U 1 1 543C768A
P 7350 3250
F 0 "HS2" H 7430 3250 40  0000 L CNN
F 1 "CONN_1" H 7350 3305 30  0001 C CNN
F 2 "opendous:1Pin_TH_40milVia" H 7350 3250 60  0001 C CNN
F 3 "" H 7350 3250 60  0000 C CNN
	1    7350 3250
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 HS3
U 1 1 543C76A6
P 7350 3350
F 0 "HS3" H 7430 3350 40  0000 L CNN
F 1 "CONN_1" H 7350 3405 30  0001 C CNN
F 2 "opendous:1Pin_TH_40milVia" H 7350 3350 60  0001 C CNN
F 3 "" H 7350 3350 60  0000 C CNN
	1    7350 3350
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 HS4
U 1 1 543C76BF
P 7350 3450
F 0 "HS4" H 7430 3450 40  0000 L CNN
F 1 "CONN_1" H 7350 3505 30  0001 C CNN
F 2 "opendous:1Pin_TH_40milVia" H 7350 3450 60  0001 C CNN
F 3 "" H 7350 3450 60  0000 C CNN
	1    7350 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3150 7100 3150
Wire Wire Line
	7100 3150 7100 3500
Wire Wire Line
	7100 3250 7200 3250
Wire Wire Line
	7100 3350 7200 3350
Connection ~ 7100 3250
Wire Wire Line
	7100 3450 7200 3450
Connection ~ 7100 3350
$Comp
L GND-RESCUE-stamp_mini_4_layers #PWR01
U 1 1 543C894D
P 7100 3500
F 0 "#PWR01" H 7100 3500 30  0001 C CNN
F 1 "GND" H 7100 3430 30  0001 C CNN
F 2 "" H 7100 3500 60  0001 C CNN
F 3 "" H 7100 3500 60  0001 C CNN
	1    7100 3500
	1    0    0    -1  
$EndComp
Connection ~ 7100 3450
$Sheet
S 1550 1750 1350 1250
U 4E78AF25
F0 "SAM3M" 60
F1 "SAM3N.sch" 60
F2 "JNRST" B R 2900 2050 60 
F3 "JTMS" B R 2900 2450 60 
F4 "JTCK" B R 2900 2350 60 
F5 "JTDO" B R 2900 2150 60 
F6 "JTDI" B R 2900 2250 60 
F7 "STM_UART_TX" B R 2900 2700 60 
F8 "STM_UART_RX" B R 2900 2800 60 
F9 "NRESET" B R 2900 2550 60 
F10 "VM" I R 2900 2900 60 
F11 "RST_ESP" I R 2900 1900 60 
$EndSheet
Wire Wire Line
	2900 2550 4000 2550
Wire Wire Line
	2900 2900 3400 2900
$Comp
L R-RESCUE-stamp_mini_4_layers RRST1
U 1 1 55F65ACF
P 3400 1900
F 0 "RRST1" V 3480 1900 50  0000 C CNN
F 1 "82k" V 3400 1900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3330 1900 30  0001 C CNN
F 3 "" H 3400 1900 30  0000 C CNN
	1    3400 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	2900 1900 3150 1900
Wire Wire Line
	3650 1900 4000 1900
$Sheet
S 1550 3450 1350 350 
U 56189EDE
F0 "power" 60
F1 "power.sch" 60
F2 "VM" I R 2900 3600 60 
$EndSheet
Wire Wire Line
	2900 3600 3400 3600
Wire Wire Line
	3400 3600 3400 2900
$EndSCHEMATC
