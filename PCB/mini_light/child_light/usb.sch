EESchema Schematic File Version 2
LIBS:con-jack
LIBS:adm3101e
LIBS:microsd
LIBS:transistor-npn
LIBS:ipc-7351-transistor
LIBS:switch-misc
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:micron_ddr_512Mb
LIBS:iMX23
LIBS:sdmmc
LIBS:usbconn
LIBS:fsusb20
LIBS:r_pack2
LIBS:pasives-connectors
LIBS:EEPROM
LIBS:PWR
LIBS:m25p32
LIBS:PROpendous-cache
LIBS:w_analog
LIBS:gl850g
LIBS:srf2012
LIBS:rclamp0502b
LIBS:mcp130
LIBS:ABM8G
LIBS:usb_a
LIBS:Reset
LIBS:stm32f100vxx
LIBS:lt1117cst
LIBS:zxmhc3f381n8
LIBS:fsusb43
LIBS:usb-mini
LIBS:ba50dd0whfp
LIBS:mic29301
LIBS:xbeepro
LIBS:tps2400
LIBS:lm3478
LIBS:mcp73812
LIBS:ftdichip
LIBS:sp481
LIBS:ref-packages
LIBS:crystal
LIBS:template_mini-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date "2 feb 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7600 3250 7350 3250
Wire Wire Line
	7350 3900 7350 3800
Connection ~ 7500 1800
Connection ~ 5450 3550
Wire Wire Line
	5450 3550 5500 3550
Wire Wire Line
	6050 4100 6050 4300
Wire Wire Line
	6050 2650 6050 3700
Wire Wire Line
	5700 3150 5800 3150
Wire Wire Line
	3300 1700 3850 1700
Wire Wire Line
	6800 2350 6700 2350
Connection ~ 3500 4850
Connection ~ 3500 4650
Connection ~ 3700 4250
Connection ~ 3700 4100
Connection ~ 3350 5300
Connection ~ 3500 5150
Connection ~ 3350 4250
Connection ~ 3500 4100
Wire Wire Line
	3500 4850 3500 5150
Wire Wire Line
	3500 4650 3350 4650
Wire Wire Line
	3350 4650 3350 5300
Wire Wire Line
	4650 4750 4750 4750
Wire Wire Line
	4750 4750 4750 4900
Wire Wire Line
	3500 3800 3500 4100
Wire Wire Line
	3500 3600 3350 3600
Wire Wire Line
	3350 3600 3350 4250
Wire Wire Line
	4650 3700 4750 3700
Wire Wire Line
	4750 3700 4750 3850
Wire Wire Line
	4800 2700 4800 2550
Wire Wire Line
	4800 2550 4700 2550
Connection ~ 3400 3100
Wire Wire Line
	3400 3100 3400 2450
Wire Wire Line
	3400 2450 3550 2450
Connection ~ 3400 1850
Wire Wire Line
	3400 1850 3400 1200
Wire Wire Line
	3400 1200 3550 1200
Wire Wire Line
	8200 850  8200 950 
Wire Wire Line
	9400 2550 9800 2550
Connection ~ 8700 800 
Wire Wire Line
	8700 800  8700 750 
Connection ~ 8700 1450
Wire Wire Line
	8700 1650 8700 1450
Wire Wire Line
	8500 1350 8500 1450
Wire Wire Line
	8500 1450 8900 1450
Wire Wire Line
	8900 1450 8900 1350
Wire Wire Line
	9650 1500 9650 2450
Wire Wire Line
	9650 2450 9400 2450
Wire Wire Line
	9800 2950 9800 3150
Wire Wire Line
	9800 3150 9400 3150
Wire Wire Line
	9700 4800 11000 4800
Wire Wire Line
	7600 5400 8800 5400
Connection ~ 8200 5400
Connection ~ 8400 5400
Wire Wire Line
	8400 5400 8400 5300
Wire Wire Line
	8800 5400 8800 5300
Wire Wire Line
	7600 5400 7600 5300
Connection ~ 8000 4850
Wire Wire Line
	8000 4900 8000 4850
Connection ~ 10100 4850
Connection ~ 10100 4800
Wire Wire Line
	10100 4800 10100 4850
Wire Wire Line
	9900 4900 9900 4850
Wire Wire Line
	9900 4850 10300 4850
Wire Wire Line
	10300 4850 10300 4900
Wire Wire Line
	10300 5400 10300 5300
Wire Wire Line
	9900 5400 10300 5400
Wire Wire Line
	9900 5400 9900 5300
Connection ~ 9550 3350
Wire Wire Line
	7600 3350 7500 3350
Wire Wire Line
	10600 3600 10600 3050
Wire Wire Line
	10600 3050 10450 3050
Wire Wire Line
	5100 4250 4600 4250
Wire Wire Line
	5100 4100 4600 4100
Wire Wire Line
	5050 5150 4550 5150
Wire Wire Line
	5050 5300 4550 5300
Wire Wire Line
	5300 3100 4800 3100
Wire Wire Line
	5300 2950 4800 2950
Connection ~ 7500 2050
Wire Wire Line
	7500 2050 7600 2050
Wire Wire Line
	7500 2950 7600 2950
Wire Wire Line
	9400 2050 10500 2050
Wire Wire Line
	9400 2150 10500 2150
Wire Wire Line
	6450 3150 7600 3150
Wire Wire Line
	6450 3050 7600 3050
Wire Wire Line
	6450 2750 7600 2750
Wire Wire Line
	6450 2850 7600 2850
Wire Wire Line
	6450 2250 7600 2250
Wire Wire Line
	5450 4100 5450 4300
Wire Wire Line
	3150 4250 3700 4250
Wire Wire Line
	5250 1700 4750 1700
Wire Wire Line
	5250 1850 4750 1850
Wire Wire Line
	3050 4100 3700 4100
Wire Wire Line
	6450 2150 7600 2150
Wire Wire Line
	9400 3350 9700 3350
Wire Wire Line
	7600 2450 7500 2450
Connection ~ 7500 2450
Connection ~ 5450 2750
Wire Wire Line
	9400 3050 9950 3050
Wire Wire Line
	7300 2350 7600 2350
Wire Wire Line
	9700 3350 9700 3750
Wire Wire Line
	10100 5400 10100 5600
Connection ~ 10100 5400
Wire Wire Line
	7600 4900 7600 4850
Wire Wire Line
	7600 4850 8800 4850
Wire Wire Line
	8800 4850 8800 4900
Wire Wire Line
	8400 4850 8400 4900
Connection ~ 8400 4850
Wire Wire Line
	8200 5400 8200 5600
Wire Wire Line
	8000 5400 8000 5300
Connection ~ 8000 5400
Wire Wire Line
	8200 4850 8200 4800
Connection ~ 8200 4800
Connection ~ 8200 4850
Wire Wire Line
	7300 4800 9100 4800
Wire Wire Line
	9400 3250 10050 3250
Wire Wire Line
	7500 1700 7500 2950
Wire Wire Line
	7500 1700 8000 1700
Wire Wire Line
	8500 950  8500 800 
Wire Wire Line
	8500 800  8900 800 
Wire Wire Line
	8900 800  8900 950 
Wire Wire Line
	8700 750  9250 750 
Wire Wire Line
	8200 1450 8200 1350
Wire Wire Line
	7600 2550 5450 2550
Wire Wire Line
	3550 1400 3550 1700
Connection ~ 3550 1700
Wire Wire Line
	3550 2650 3550 2950
Connection ~ 3550 2950
Wire Wire Line
	4700 1300 4800 1300
Wire Wire Line
	4800 1300 4800 1400
Connection ~ 6050 2750
Wire Wire Line
	6050 2650 7600 2650
Wire Wire Line
	7500 3350 7500 3400
Wire Wire Line
	3000 5300 3650 5300
Wire Wire Line
	5750 3150 5750 3250
Connection ~ 5750 3150
Wire Wire Line
	5450 2550 5450 3700
Wire Wire Line
	6050 3550 6000 3550
Connection ~ 6050 3550
Wire Wire Line
	2900 5150 3650 5150
Wire Wire Line
	7350 3250 7350 3400
Wire Wire Line
	6850 3250 6800 3250
Text HLabel 6800 3250 0    60   BiDi ~ 0
USB_reset
$Comp
L C C9
U 1 1 4F522649
P 7350 3600
F 0 "C9" H 7400 3700 50  0000 L CNN
F 1 "0.1uF" H 7400 3500 50  0000 L CNN
F 2 "SM0603" H 7350 3600 60  0001 C CNN
F 3 "" H 7350 3600 60  0001 C CNN
	1    7350 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 4F522478
P 7350 3900
F 0 "#PWR014" H 7350 3900 30  0001 C CNN
F 1 "GND" H 7350 3830 30  0001 C CNN
F 2 "" H 7350 3900 60  0001 C CNN
F 3 "" H 7350 3900 60  0001 C CNN
	1    7350 3900
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 4F522458
P 7100 3250
F 0 "R3" V 7200 3250 50  0000 C CNN
F 1 "10k" V 7100 3250 50  0000 C CNN
F 2 "SM0603" H 7100 3250 60  0001 C CNN
F 3 "" H 7100 3250 60  0001 C CNN
	1    7100 3250
	0    1    1    0   
$EndComp
$Comp
L GND #PWR016
U 1 1 4E43139E
P 5750 3250
F 0 "#PWR016" H 5750 3250 30  0001 C CNN
F 1 "GND" H 5750 3180 30  0001 C CNN
F 2 "" H 5750 3250 60  0001 C CNN
F 3 "" H 5750 3250 60  0001 C CNN
	1    5750 3250
	1    0    0    -1  
$EndComp
$Comp
L ABM8G X1
U 1 1 4E4310B7
P 5750 2750
F 0 "X1" H 5750 2900 60  0000 C CNN
F 1 "12MHz" H 5750 2600 60  0000 C CNN
F 2 "ABM8G" H 5750 2750 60  0001 C CNN
F 3 "" H 5750 2750 60  0001 C CNN
	1    5750 2750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 4E41C8A5
P 7500 3400
F 0 "#PWR017" H 7500 3400 30  0001 C CNN
F 1 "GND" H 7500 3330 30  0001 C CNN
F 2 "" H 7500 3400 60  0001 C CNN
F 3 "" H 7500 3400 60  0001 C CNN
	1    7500 3400
	1    0    0    -1  
$EndComp
Text Label 6300 2650 0    60   ~ 0
X2
NoConn ~ 9400 2650
NoConn ~ 9400 2750
NoConn ~ 9400 2850
NoConn ~ 9400 2950
$Comp
L RCLAMP0502B U3
U 1 1 4E4157F9
P 4500 4450
F 0 "U3" V 4500 5350 60  0000 C CNN
F 1 "RCLAMP0502B" V 4500 4850 60  0000 C CNN
F 2 "SOT-523" H 4500 4450 60  0001 C CNN
F 3 "" H 4500 4450 60  0001 C CNN
	1    4500 4450
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR018
U 1 1 4E4157F8
P 4750 4900
F 0 "#PWR018" H 4750 4900 30  0001 C CNN
F 1 "GND" H 4750 4830 30  0001 C CNN
F 2 "" H 4750 4900 60  0001 C CNN
F 3 "" H 4750 4900 60  0001 C CNN
	1    4750 4900
	1    0    0    -1  
$EndComp
$Comp
L RCLAMP0502B U2
U 1 1 4E4157EA
P 4500 3400
F 0 "U2" V 4500 4300 60  0000 C CNN
F 1 "RCLAMP0502B" V 4500 3800 60  0000 C CNN
F 2 "SOT-523" H 4500 3400 60  0001 C CNN
F 3 "" H 4500 3400 60  0001 C CNN
	1    4500 3400
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR019
U 1 1 4E4157E9
P 4750 3850
F 0 "#PWR019" H 4750 3850 30  0001 C CNN
F 1 "GND" H 4750 3780 30  0001 C CNN
F 2 "" H 4750 3850 60  0001 C CNN
F 3 "" H 4750 3850 60  0001 C CNN
	1    4750 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 4E4157C0
P 4800 2700
F 0 "#PWR020" H 4800 2700 30  0001 C CNN
F 1 "GND" H 4800 2630 30  0001 C CNN
F 2 "" H 4800 2700 60  0001 C CNN
F 3 "" H 4800 2700 60  0001 C CNN
	1    4800 2700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 4E4157B3
P 4800 1400
F 0 "#PWR021" H 4800 1400 30  0001 C CNN
F 1 "GND" H 4800 1330 30  0001 C CNN
F 2 "" H 4800 1400 60  0001 C CNN
F 3 "" H 4800 1400 60  0001 C CNN
	1    4800 1400
	1    0    0    -1  
$EndComp
$Comp
L RCLAMP0502B U5
U 1 1 4E41574D
P 4550 2250
F 0 "U5" V 4550 3150 60  0000 C CNN
F 1 "RCLAMP0502B" V 4550 2650 60  0000 C CNN
F 2 "SOT-523" H 4550 2250 60  0001 C CNN
F 3 "" H 4550 2250 60  0001 C CNN
	1    4550 2250
	0    -1   -1   0   
$EndComp
$Comp
L RCLAMP0502B U4
U 1 1 4E415735
P 4550 1000
F 0 "U4" V 4500 1800 60  0000 C CNN
F 1 "RCLAMP0502B" V 4500 1400 60  0000 C CNN
F 2 "SOT-523" H 4550 1000 60  0001 C CNN
F 3 "" H 4550 1000 60  0001 C CNN
	1    4550 1000
	0    -1   -1   0   
$EndComp
$Comp
L C C12
U 1 1 4E3C4442
P 8200 1150
F 0 "C12" H 8250 1250 50  0000 L CNN
F 1 "100nF" H 8250 1050 50  0000 L CNN
F 2 "SM0603" H 8200 1150 60  0001 C CNN
F 3 "" H 8200 1150 60  0001 C CNN
	1    8200 1150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 4E3C4430
P 8200 1450
F 0 "#PWR022" H 8200 1450 30  0001 C CNN
F 1 "GND" H 8200 1380 30  0001 C CNN
F 2 "" H 8200 1450 60  0001 C CNN
F 3 "" H 8200 1450 60  0001 C CNN
	1    8200 1450
	1    0    0    -1  
$EndComp
$Comp
L VDD5V #PWR023
U 1 1 4E3C4429
P 8200 850
F 0 "#PWR023" H 8200 950 30  0001 C CNN
F 1 "VDD5V" H 8200 950 30  0000 C CNN
F 2 "" H 8200 850 60  0001 C CNN
F 3 "" H 8200 850 60  0001 C CNN
	1    8200 850 
	1    0    0    -1  
$EndComp
$Comp
L VDD5V #PWR024
U 1 1 4E3C4418
P 9800 2550
F 0 "#PWR024" H 9800 2650 30  0001 C CNN
F 1 "VDD5V" H 9800 2650 30  0000 C CNN
F 2 "" H 9800 2550 60  0001 C CNN
F 3 "" H 9800 2550 60  0001 C CNN
	1    9800 2550
	1    0    0    -1  
$EndComp
Text Label 9650 1500 0    60   ~ 0
USBH_P33VD
$Comp
L C C14
U 1 1 4E3C436D
P 8500 1150
F 0 "C14" H 8550 1250 50  0000 L CNN
F 1 "4.7uF" H 8500 1050 50  0000 L CNN
F 2 "SM1206" H 8500 1150 60  0001 C CNN
F 3 "" H 8500 1150 60  0001 C CNN
	1    8500 1150
	1    0    0    -1  
$EndComp
$Comp
L C C16
U 1 1 4E3C436C
P 8900 1150
F 0 "C16" H 8950 1250 50  0000 L CNN
F 1 "100nF" H 8950 1050 50  0000 L CNN
F 2 "SM0603" H 8900 1150 60  0001 C CNN
F 3 "" H 8900 1150 60  0001 C CNN
	1    8900 1150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR025
U 1 1 4E3C436B
P 8700 1650
F 0 "#PWR025" H 8700 1650 30  0001 C CNN
F 1 "GND" H 8700 1580 30  0001 C CNN
F 2 "" H 8700 1650 60  0001 C CNN
F 3 "" H 8700 1650 60  0001 C CNN
	1    8700 1650
	1    0    0    -1  
$EndComp
Text Label 8700 750  0    60   ~ 0
USBH_P33VD
Text Label 7500 1700 0    60   ~ 0
USBH_P33V
$Comp
L VDD5V #PWR026
U 1 1 4E3C4207
P 9800 2950
F 0 "#PWR026" H 9800 3050 30  0001 C CNN
F 1 "VDD5V" H 9800 3050 30  0000 C CNN
F 2 "" H 9800 2950 60  0001 C CNN
F 3 "" H 9800 2950 60  0001 C CNN
	1    9800 2950
	1    0    0    -1  
$EndComp
Text Label 9500 3250 0    60   ~ 0
USBH_P33VD
Text Label 10450 4800 0    60   ~ 0
USBH_P33VD
Text Label 7300 4800 0    60   ~ 0
USBH_P33V
$Comp
L GND #PWR027
U 1 1 4E3C3A60
P 10100 5600
F 0 "#PWR027" H 10100 5600 30  0001 C CNN
F 1 "GND" H 10100 5530 30  0001 C CNN
F 2 "" H 10100 5600 60  0001 C CNN
F 3 "" H 10100 5600 60  0001 C CNN
	1    10100 5600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR028
U 1 1 4E3C3A56
P 8200 5600
F 0 "#PWR028" H 8200 5600 30  0001 C CNN
F 1 "GND" H 8200 5530 30  0001 C CNN
F 2 "" H 8200 5600 60  0001 C CNN
F 3 "" H 8200 5600 60  0001 C CNN
	1    8200 5600
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR L8
U 1 1 4E3C3A1D
P 9400 4800
F 0 "L8" V 9350 4800 40  0000 C CNN
F 1 "2A 120 ohm" V 9500 4800 40  0000 C CNN
F 2 "SM0805" H 9400 4800 60  0001 C CNN
F 3 "" H 9400 4800 60  0001 C CNN
	1    9400 4800
	0    1    1    0   
$EndComp
$Comp
L C C18
U 1 1 4E3C3993
P 10300 5100
F 0 "C18" H 10350 5200 50  0000 L CNN
F 1 "100nF" H 10350 5000 50  0000 L CNN
F 2 "SM0603" H 10300 5100 60  0001 C CNN
F 3 "" H 10300 5100 60  0001 C CNN
	1    10300 5100
	1    0    0    -1  
$EndComp
$Comp
L C C17
U 1 1 4E3C3992
P 9900 5100
F 0 "C17" H 9950 5200 50  0000 L CNN
F 1 "10nF" H 9900 5000 50  0000 L CNN
F 2 "SM0603" H 9900 5100 60  0001 C CNN
F 3 "" H 9900 5100 60  0001 C CNN
	1    9900 5100
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 4E3C3979
P 7600 5100
F 0 "C10" H 7650 5200 50  0000 L CNN
F 1 "10nF" H 7600 5000 50  0000 L CNN
F 2 "SM0603" H 7600 5100 60  0001 C CNN
F 3 "" H 7600 5100 60  0001 C CNN
	1    7600 5100
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 4E3C3978
P 8000 5100
F 0 "C11" H 8050 5200 50  0000 L CNN
F 1 "100nF" H 8050 5000 50  0000 L CNN
F 2 "SM0603" H 8000 5100 60  0001 C CNN
F 3 "" H 8000 5100 60  0001 C CNN
	1    8000 5100
	1    0    0    -1  
$EndComp
$Comp
L C C15
U 1 1 4E3C3964
P 8800 5100
F 0 "C15" H 8850 5200 50  0000 L CNN
F 1 "100nF" H 8850 5000 50  0000 L CNN
F 2 "SM0603" H 8800 5100 60  0001 C CNN
F 3 "" H 8800 5100 60  0001 C CNN
	1    8800 5100
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 4E3C3916
P 8400 5100
F 0 "C13" H 8450 5200 50  0000 L CNN
F 1 "100nF" H 8400 5000 50  0000 L CNN
F 2 "SM0603" H 8400 5100 60  0001 C CNN
F 3 "" H 8400 5100 60  0001 C CNN
	1    8400 5100
	1    0    0    -1  
$EndComp
Text Notes 7550 7550 0    60   ~ 0
andres.calderon@emqbit.com\n
$Comp
L GND #PWR029
U 1 1 4E3C3807
P 6700 2350
F 0 "#PWR029" H 6700 2350 30  0001 C CNN
F 1 "GND" H 6700 2280 30  0001 C CNN
F 2 "" H 6700 2350 60  0001 C CNN
F 3 "" H 6700 2350 60  0001 C CNN
	1    6700 2350
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 4E3C37F3
P 7050 2350
F 0 "R2" V 6950 2600 50  0000 C CNN
F 1 "680 1%" V 6950 2350 50  0000 C CNN
F 2 "SM0603" H 7050 2350 60  0001 C CNN
F 3 "" H 7050 2350 60  0001 C CNN
	1    7050 2350
	0    -1   -1   0   
$EndComp
$Comp
L R R4
U 1 1 4E3C36E7
P 10200 3050
F 0 "R4" V 10280 3050 50  0000 C CNN
F 1 "100k" V 10100 3050 50  0000 C CNN
F 2 "SM0603" H 10200 3050 60  0001 C CNN
F 3 "" H 10200 3050 60  0001 C CNN
	1    10200 3050
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 4E3C36D9
P 5750 3550
F 0 "R1" V 5830 3550 50  0000 C CNN
F 1 "1M" V 5650 3550 50  0000 C CNN
F 2 "SM0603" H 5750 3550 60  0001 C CNN
F 3 "" H 5750 3550 60  0001 C CNN
	1    5750 3550
	0    1    1    0   
$EndComp
$Comp
L GND #PWR030
U 1 1 4E3C36BE
P 10600 3600
F 0 "#PWR030" H 10600 3600 30  0001 C CNN
F 1 "GND" H 10600 3530 30  0001 C CNN
F 2 "" H 10600 3600 60  0001 C CNN
F 3 "" H 10600 3600 60  0001 C CNN
	1    10600 3600
	1    0    0    -1  
$EndComp
Text Label 4700 4100 0    60   ~ 0
USB_DM3
Text Label 4700 4250 0    60   ~ 0
USB_DP3
Text Label 4650 5300 0    60   ~ 0
USB_DP4
Text Label 4650 5150 0    60   ~ 0
USB_DM4
Text Label 4900 2950 0    60   ~ 0
USB_DM2
Text Label 4900 3100 0    60   ~ 0
USB_DP2
Text Label 4850 1850 0    60   ~ 0
USB_DP1
Text Label 4850 1700 0    60   ~ 0
USB_DM1
Text Label 6200 2550 0    60   ~ 0
X1
$Comp
L GND #PWR031
U 1 1 4E3C222B
P 9700 3750
F 0 "#PWR031" H 9700 3750 30  0001 C CNN
F 1 "GND" H 9700 3680 30  0001 C CNN
F 2 "" H 9700 3750 60  0001 C CNN
F 3 "" H 9700 3750 60  0001 C CNN
	1    9700 3750
	1    0    0    -1  
$EndComp
Text Label 10100 2150 0    60   ~ 0
USB_DM1
Text Label 10100 2050 0    60   ~ 0
USB_DP1
Text Label 6450 3150 0    60   ~ 0
USB_DP4
Text Label 6450 3050 0    60   ~ 0
USB_DM4
Text Label 6450 2750 0    60   ~ 0
USB_DM3
Text Label 6450 2850 0    60   ~ 0
USB_DP3
Text Label 6450 2250 0    60   ~ 0
USB_DP2
Text Label 6450 2150 0    60   ~ 0
USB_DM2
$Comp
L GND #PWR032
U 1 1 4E3C212B
P 5450 4300
F 0 "#PWR032" H 5450 4300 30  0001 C CNN
F 1 "GND" H 5450 4230 30  0001 C CNN
F 2 "" H 5450 4300 60  0001 C CNN
F 3 "" H 5450 4300 60  0001 C CNN
	1    5450 4300
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 4E3C20C6
P 5450 3900
F 0 "C7" H 5500 4000 50  0000 L CNN
F 1 "8pF" H 5500 3800 50  0000 L CNN
F 2 "SM0603" H 5450 3900 60  0001 C CNN
F 3 "" H 5450 3900 60  0001 C CNN
	1    5450 3900
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 4E3C208F
P 6050 3900
F 0 "C8" H 6100 4000 50  0000 L CNN
F 1 "8pF" H 6100 3800 50  0000 L CNN
F 2 "SM0603" H 6050 3900 60  0001 C CNN
F 3 "" H 6050 3900 60  0001 C CNN
	1    6050 3900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR033
U 1 1 4E3C206C
P 6050 4300
F 0 "#PWR033" H 6050 4300 30  0001 C CNN
F 1 "GND" H 6050 4230 30  0001 C CNN
F 2 "" H 6050 4300 60  0001 C CNN
F 3 "" H 6050 4300 60  0001 C CNN
	1    6050 4300
	1    0    0    -1  
$EndComp
$Comp
L SRF2012 L4
U 1 1 4E3C1756
P 4100 5200
F 0 "L4" H 4100 5050 60  0000 C CNN
F 1 "SRF2012" H 4100 5300 60  0000 C CNN
F 2 "SRF2012" H 4100 5200 60  0001 C CNN
F 3 "" H 4100 5200 60  0001 C CNN
	1    4100 5200
	1    0    0    -1  
$EndComp
$Comp
L SRF2012 L5
U 1 1 4E3C1750
P 4150 4150
F 0 "L5" H 4150 4000 60  0000 C CNN
F 1 "SRF2012" H 4150 4250 60  0000 C CNN
F 2 "SRF2012" H 4150 4150 60  0001 C CNN
F 3 "" H 4150 4150 60  0001 C CNN
	1    4150 4150
	1    0    0    -1  
$EndComp
$Comp
L SRF2012 L7
U 1 1 4E3C173D
P 4350 3000
F 0 "L7" H 4350 2850 60  0000 C CNN
F 1 "SRF2012" H 4350 3100 60  0000 C CNN
F 2 "SRF2012" H 4350 3000 60  0001 C CNN
F 3 "" H 4350 3000 60  0001 C CNN
	1    4350 3000
	1    0    0    -1  
$EndComp
$Comp
L SRF2012 L6
U 1 1 4E3C172F
P 4300 1750
F 0 "L6" H 4300 1600 60  0000 C CNN
F 1 "SRF2012" H 4300 1850 60  0000 C CNN
F 2 "SRF2012" H 4300 1750 60  0001 C CNN
F 3 "" H 4300 1750 60  0001 C CNN
	1    4300 1750
	1    0    0    -1  
$EndComp
$Comp
L GL850G U6
U 1 1 4E3BF185
P 8500 2700
F 0 "U6" H 8300 3300 60  0000 C CNN
F 1 "GL850G" H 8400 3200 60  0000 C CNN
F 2 "SSOP28" H 8500 2700 60  0001 C CNN
F 3 "" H 8500 2700 60  0001 C CNN
	1    8500 2700
	1    0    0    -1  
$EndComp
Text HLabel 10050 2250 2    60   3State ~ 0
USB_DP0
Text HLabel 10050 2350 2    60   3State ~ 0
USB_DM0
Wire Wire Line
	9400 2250 10050 2250
Wire Wire Line
	9400 2350 10050 2350
Wire Wire Line
	2100 5050 2100 5650
Connection ~ 2100 5050
Connection ~ 2100 5350
Connection ~ 2100 5250
Connection ~ 2100 5150
Connection ~ 1450 5400
Wire Wire Line
	1050 5400 1800 5400
Wire Wire Line
	1050 5400 1050 5300
Connection ~ 1600 5400
Wire Wire Line
	1600 5400 1600 5500
Connection ~ 1800 4800
Wire Wire Line
	2900 5350 2900 5550
Wire Wire Line
	1450 4900 1450 4800
Wire Wire Line
	1450 5400 1450 5300
Wire Wire Line
	1800 5400 1800 5300
Wire Wire Line
	1050 4900 1050 4800
Connection ~ 1450 4800
Wire Wire Line
	1800 4650 1800 4900
Wire Wire Line
	1200 4650 1150 4650
Wire Wire Line
	1150 4650 1150 4600
Wire Wire Line
	2900 5250 3000 5250
Wire Wire Line
	2900 4800 2900 5050
Wire Wire Line
	1050 4800 2900 4800
Text Notes 2200 5550 0    60   ~ 0
UE27AC54100
Text GLabel 2150 5650 2    60   BiDi ~ 0
GND_CASE
$Comp
L USB_2A J2
U 1 1 51B8EA61
P 2700 5200
F 0 "J2" H 2625 5450 60  0000 C CNN
F 1 "USB_2A" H 2750 4900 60  0001 C CNN
F 2 "CONN_USB-A_Vertical_Opendous" H 2700 5200 60  0001 C CNN
F 3 "" H 2700 5200 60  0001 C CNN
F 4 "VCC" H 3025 5350 50  0001 C CNN "VCC"
F 5 "D+" H 3000 5250 50  0001 C CNN "Data+"
F 6 "D-" H 3000 5150 50  0001 C CNN "Data-"
F 7 "GND" H 3025 5050 50  0001 C CNN "Ground"
	1    2700 5200
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR L2
U 1 1 51B8EA67
P 1500 4650
F 0 "L2" V 1450 4650 40  0000 C CNN
F 1 "2A 120 ohm" V 1600 4650 40  0000 C CNN
F 2 "SM0805" H 1500 4650 60  0001 C CNN
F 3 "" H 1500 4650 60  0001 C CNN
	1    1500 4650
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D2
U 1 1 51B8EA6D
P 1050 5100
F 0 "D2" H 1050 5200 40  0000 C CNN
F 1 "RSA5M" H 1050 5000 40  0000 C CNN
F 2 "RSA5M" H 1050 5100 60  0001 C CNN
F 3 "" H 1050 5100 60  0001 C CNN
	1    1050 5100
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR034
U 1 1 51B8EA73
P 1600 5500
F 0 "#PWR034" H 1600 5500 30  0001 C CNN
F 1 "GND" H 1600 5430 30  0001 C CNN
F 2 "" H 1600 5500 60  0001 C CNN
F 3 "" H 1600 5500 60  0001 C CNN
	1    1600 5500
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 51B8EA79
P 1450 5100
F 0 "C2" H 1500 5200 50  0000 L CNN
F 1 "33uF" H 1500 5000 50  0000 L CNN
F 2 "SM1206" H 1450 5100 60  0001 C CNN
F 3 "" H 1450 5100 60  0001 C CNN
	1    1450 5100
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 51B8EA7F
P 1800 5100
F 0 "C5" H 1850 5200 50  0000 L CNN
F 1 "100nF" H 1850 5000 50  0000 L CNN
F 2 "SM0603" H 1800 5100 60  0001 C CNN
F 3 "" H 1800 5100 60  0001 C CNN
	1    1800 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR035
U 1 1 51B8EA85
P 2900 5550
F 0 "#PWR035" H 2900 5550 30  0001 C CNN
F 1 "GND" H 2900 5480 30  0001 C CNN
F 2 "" H 2900 5550 60  0001 C CNN
F 3 "" H 2900 5550 60  0001 C CNN
	1    2900 5550
	1    0    0    -1  
$EndComp
$Comp
L VDD5V #PWR036
U 1 1 51B8EA8B
P 1300 3550
F 0 "#PWR036" H 1300 3650 30  0001 C CNN
F 1 "VDD5V" H 1300 3650 30  0000 C CNN
F 2 "" H 1300 3550 60  0001 C CNN
F 3 "" H 1300 3550 60  0001 C CNN
	1    1300 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4000 2250 4600
Connection ~ 2250 4000
Connection ~ 2250 4300
Connection ~ 2250 4200
Connection ~ 2250 4100
Connection ~ 1600 4350
Wire Wire Line
	1200 4350 1950 4350
Wire Wire Line
	1200 4350 1200 4250
Connection ~ 1750 4350
Wire Wire Line
	1750 4350 1750 4450
Connection ~ 1950 3750
Wire Wire Line
	3050 4300 3050 4500
Wire Wire Line
	1600 3850 1600 3750
Wire Wire Line
	1600 4350 1600 4250
Wire Wire Line
	1950 4350 1950 4250
Wire Wire Line
	1200 3850 1200 3750
Connection ~ 1600 3750
Wire Wire Line
	1950 3600 1950 3850
Wire Wire Line
	1350 3600 1300 3600
Wire Wire Line
	1300 3600 1300 3550
Wire Wire Line
	3050 4200 3150 4200
Wire Wire Line
	3050 3750 3050 4000
Wire Wire Line
	1200 3750 3050 3750
Text Notes 2350 4500 0    60   ~ 0
UE27AC54100
Text GLabel 9400 6200 0    60   BiDi ~ 0
GND_CASE
$Comp
L USB_2A J1
U 1 1 51B8EAB7
P 2850 4150
F 0 "J1" H 2775 4400 60  0000 C CNN
F 1 "USB_2A" H 2900 3850 60  0001 C CNN
F 2 "CONN_USB-A_Vertical_Opendous" H 2850 4150 60  0001 C CNN
F 3 "" H 2850 4150 60  0001 C CNN
F 4 "VCC" H 3175 4300 50  0001 C CNN "VCC"
F 5 "D+" H 3150 4200 50  0001 C CNN "Data+"
F 6 "D-" H 3150 4100 50  0001 C CNN "Data-"
F 7 "GND" H 3175 4000 50  0001 C CNN "Ground"
	1    2850 4150
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR L1
U 1 1 51B8EABD
P 1650 3600
F 0 "L1" V 1600 3600 40  0000 C CNN
F 1 "2A 120 ohm" V 1750 3600 40  0000 C CNN
F 2 "SM0805" H 1650 3600 60  0001 C CNN
F 3 "" H 1650 3600 60  0001 C CNN
	1    1650 3600
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D1
U 1 1 51B8EAC3
P 1200 4050
F 0 "D1" H 1200 4150 40  0000 C CNN
F 1 "RSA5M" H 1200 3950 40  0000 C CNN
F 2 "RSA5M" H 1200 4050 60  0001 C CNN
F 3 "" H 1200 4050 60  0001 C CNN
	1    1200 4050
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR037
U 1 1 51B8EAC9
P 1750 4450
F 0 "#PWR037" H 1750 4450 30  0001 C CNN
F 1 "GND" H 1750 4380 30  0001 C CNN
F 2 "" H 1750 4450 60  0001 C CNN
F 3 "" H 1750 4450 60  0001 C CNN
	1    1750 4450
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 51B8EACF
P 1600 4050
F 0 "C1" H 1650 4150 50  0000 L CNN
F 1 "33uF" H 1650 3950 50  0000 L CNN
F 2 "SM1206" H 1600 4050 60  0001 C CNN
F 3 "" H 1600 4050 60  0001 C CNN
	1    1600 4050
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 51B8EAD5
P 1950 4050
F 0 "C4" H 2000 4150 50  0000 L CNN
F 1 "100nF" H 2000 3950 50  0000 L CNN
F 2 "SM0603" H 1950 4050 60  0001 C CNN
F 3 "" H 1950 4050 60  0001 C CNN
	1    1950 4050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR038
U 1 1 51B8EADB
P 3050 4500
F 0 "#PWR038" H 3050 4500 30  0001 C CNN
F 1 "GND" H 3050 4430 30  0001 C CNN
F 2 "" H 3050 4500 60  0001 C CNN
F 3 "" H 3050 4500 60  0001 C CNN
	1    3050 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 5650 2150 5650
Wire Wire Line
	2250 4600 2300 4600
Wire Wire Line
	3000 5250 3000 5300
Wire Wire Line
	3150 4200 3150 4250
$Comp
L +5VL #PWR14
U 1 1 51B93FDC
P 1150 4600
F 0 "#PWR14" H 1150 4730 20  0001 C CNN
F 1 "+5VL" H 1150 4700 30  0000 C CNN
F 2 "" H 1150 4600 60  0000 C CNN
F 3 "" H 1150 4600 60  0000 C CNN
	1    1150 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2950 3300 2950
Wire Wire Line
	3900 3100 3300 3100
Text HLabel 3300 2950 0    60   BiDi ~ 0
DM4
Text HLabel 3300 3100 0    60   BiDi ~ 0
DP4
Wire Wire Line
	3300 1850 3850 1850
Wire Wire Line
	10000 6200 10100 6200
Wire Wire Line
	10100 6200 10100 6350
$Comp
L GND #PWR039
U 1 1 520C8C3D
P 10100 6350
F 0 "#PWR039" H 10100 6350 30  0001 C CNN
F 1 "GND" H 10100 6280 30  0001 C CNN
F 2 "" H 10100 6350 60  0001 C CNN
F 3 "" H 10100 6350 60  0001 C CNN
	1    10100 6350
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR L3
U 1 1 520C8C43
P 9700 6200
F 0 "L3" V 9650 6200 40  0000 C CNN
F 1 "HZ0805D152R-10" V 9800 6200 40  0000 C CNN
F 2 "SM0805" H 9700 6200 60  0001 C CNN
F 3 "" H 9700 6200 60  0001 C CNN
	1    9700 6200
	0    -1   -1   0   
$EndComp
Text GLabel 2300 4600 2    60   BiDi ~ 0
GND_CASE
$EndSCHEMATC
