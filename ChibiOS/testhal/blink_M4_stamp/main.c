#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include <math.h>

static WORKING_AREA(waThread1, 128);
static msg_t Thread1(void *arg) {

    (void)arg;
    chRegSetThreadName("blinker");
    while (TRUE) {
        palSetPad(GPIOD, GPIO_LED);
        chThdSleepMilliseconds(500);
        palClearPad(GPIOD, GPIO_LED);
        chThdSleepMilliseconds(500);
    }
  return(0);
}


/*
 * Application entry point.
 */
int main(void) {

    /*
     * System initializations.
     * - HAL initialization, this also initializes the configured device drivers
     *   and performs the board-specific initializations.
     * - Kernel initialization, the main() function becomes a thread and the
     *   RTOS is active.
     */
    halInit();
    chSysInit();

  /*
   * Activates the serial driver 1 using the driver default configuration.
   */
  sdStart(&SD1, NULL);
  
  
    /*
     * Creates the blinker thread.
     */
       chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

    /*
     * Normal main() thread activity, in this demo it does nothing.
     */

    while (TRUE) {
		TestThread(&SD1);
        chThdSleepMilliseconds(1000);
    }
}
