/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"

#include <realfft_split.h>

#define ADC_GRP2_NUM_CHANNELS   8
#define ADC_GRP2_BUF_DEPTH      1024

static adcsample_t samples0[ADC_GRP2_NUM_CHANNELS * ADC_GRP2_BUF_DEPTH];
static adcsample_t samples1[ADC_GRP2_NUM_CHANNELS * ADC_GRP2_BUF_DEPTH];

static void adccallback(ADCDriver *adcp, adcsample_t *buffer, size_t n);


static void adcerrorcallback(ADCDriver *adcp, adcerror_t err) {

  (void)adcp;
  (void)err;
}

/*
 * ADC conversion group.
 * Mode:        Continuous, 16 samples of 8 channels, SW triggered.
 * Channels:    IN11, IN12, IN11, IN12, IN11, IN12, Sensor, VRef.
 */
#define ADC_SAMPLE_PERIOD ADC_SAMPLE_3

static const ADCConversionGroup adcgrpcfg2 = {
  TRUE,
  ADC_GRP2_NUM_CHANNELS,
  adccallback,
  adcerrorcallback,
  0,                        /* CR1 */
  ADC_CR2_SWSTART,          /* CR2 */
  0, /* SMPR2 */
  ADC_SMPR2_SMP_AN0(ADC_SAMPLE_PERIOD) |                         
  ADC_SMPR2_SMP_AN1(ADC_SAMPLE_PERIOD) |
  ADC_SMPR2_SMP_AN2(ADC_SAMPLE_PERIOD) |
  ADC_SMPR2_SMP_AN3(ADC_SAMPLE_PERIOD) |
  ADC_SMPR2_SMP_AN4(ADC_SAMPLE_PERIOD) |
  ADC_SMPR2_SMP_AN5(ADC_SAMPLE_PERIOD) |
  ADC_SMPR2_SMP_AN6(ADC_SAMPLE_PERIOD) |
  ADC_SMPR2_SMP_AN7(ADC_SAMPLE_PERIOD), /* SMPR2 */
  ADC_SQR1_NUM_CH(ADC_GRP2_NUM_CHANNELS),
  ADC_SQR2_SQ8_N(ADC_CHANNEL_IN8)  | ADC_SQR2_SQ7_N(ADC_CHANNEL_IN7),
  ADC_SQR3_SQ6_N(ADC_CHANNEL_IN6)  | ADC_SQR3_SQ5_N(ADC_CHANNEL_IN5) |
  ADC_SQR3_SQ4_N(ADC_CHANNEL_IN4)  | ADC_SQR3_SQ3_N(ADC_CHANNEL_IN3) |
  ADC_SQR3_SQ2_N(ADC_CHANNEL_IN2)  | ADC_SQR3_SQ1_N(ADC_CHANNEL_IN1)
};

/*
 * ADC streaming callback.
 */
static void adccallback(ADCDriver *adcp, adcsample_t *buffer, size_t n) {

  (void)adcp;
  
  static int double_buffer_ndx = 0;
  
  switch(double_buffer_ndx)
  {
    case 0:
      adcStartConversion(&ADCD1, &adcgrpcfg2, samples0, ADC_GRP2_BUF_DEPTH);
      double_buffer_ndx = 1;
      break;
      
    case 1:
      adcStartConversion(&ADCD1, &adcgrpcfg2, samples1, ADC_GRP2_BUF_DEPTH);
      double_buffer_ndx = 0;
      break;
      
    default: //paranoic case
      double_buffer_ndx = 0;
  }
}

/*
 * Red LEDs blinker thread, times are in milliseconds.
 */
static WORKING_AREA(waThread1, 128);
static msg_t Thread1(void *arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (TRUE) {
    palSetPad(GPIOD, GPIOD_LED5);
    chThdSleepMilliseconds(500);
    palClearPad(GPIOD, GPIOD_LED5);
    chThdSleepMilliseconds(500);
  }
}

int
testFFT ()
{
  int i;
  float data[N * 2];
  float *real = data;
  float *imag = &data[N / 2];

  for (i = 0; i < N; i++)
    data[i] = i;

  init_realfft_split ();
  realfft_split (data);
  
  chprintf((BaseSequentialStream *)&SD2, "%d %f %f\n ", 0, real[0], 0);
  for (i = 1; i < N / 2; i++)
    chprintf((BaseSequentialStream *)&SD2,"%d %f %f\n ", i, real[i], imag[N / 2 - i]);

  return 0;
}

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Setting up analog inputs used by the demo.
   */
  palSetGroupMode(GPIOC, PAL_PORT_BIT(1) | PAL_PORT_BIT(2),
                  0, PAL_MODE_INPUT_ANALOG);

  /*
   * Creates the blinker thread.
   */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

  /*
   * Activates the ADC1 driver and the thermal sensor.
   */
  adcStart(&ADCD1, NULL);
  adcSTM32EnableTSVREFE();


  sdStart(&SD2, NULL);
  
  /*
   * Starts an ADC continuous conversion.
   */
  adcStartConversion(&ADCD1, &adcgrpcfg2, samples0, ADC_GRP2_BUF_DEPTH);

  
    
  testFFT ();
  /*
   * Normal main() thread activity, in this demo it does nothing.
   */
  while (TRUE) {
    if (palReadPad(GPIOA, GPIOA_BUTTON)) {
      adcStopConversion(&ADCD1);
      adcSTM32DisableTSVREFE();
    }
    chThdSleepMilliseconds(500);
  }
}
