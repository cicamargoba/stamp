#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "efc.h"
#include "flashd.h"


#include "py/nlr.h"
#include "py/compile.h"
#include "py/runtime.h"
#include "py/repl.h"

#include "mpconfig.h"
#include "misc.h"
#include "qstr.h"
#include "misc.h"
#include "nlr.h"
#include "lexer.h"
#include "parse.h"
#include "obj.h"
#include "runtime.h"
#include "stackctrl.h"
#include "gc.h"

#define BOARD_MCK               48000000
//static WORKING_AREA(waThread1, 128);


void do_str(const char *src) {
    mp_lexer_t *lex = mp_lexer_new_from_str_len(MP_QSTR__lt_stdin_gt_, src, strlen(src), 0);
    if (lex == NULL) {
        return;
    }

    nlr_buf_t nlr;
    if (nlr_push(&nlr) == 0) {
        qstr source_name = lex->source_name;
        mp_parse_node_t pn = mp_parse(lex, MP_PARSE_SINGLE_INPUT);
        mp_obj_t module_fun = mp_compile(pn, source_name, MP_EMIT_OPT_NONE, true);
        mp_call_function_0(module_fun);
        nlr_pop();
    } else {
        // uncaught exception
        mp_obj_print_exception(&mp_plat_print, (mp_obj_t)nlr.ret_val);
    }
}

mp_import_stat_t mp_import_stat(const char *path) {
    return MP_IMPORT_STAT_NO_EXIST;
}

mp_lexer_t *mp_lexer_new_from_file(const char *filename) {
    return NULL;
}

mp_obj_t mp_builtin_open(uint n_args, const mp_obj_t *args, mp_map_t *kwargs) {
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_KW(mp_builtin_open_obj, 1, mp_builtin_open);

/*
 * Application entry point.
 */
int main(void) {

  halInit();
  chSysInit();

  sdStart(&SD2, NULL);  /* Activates the serial driver 2 */
  sdStart(&SD1, NULL);  /* Activates the serial driver 1 */


    mp_init();
    mp_obj_list_init(mp_sys_path, 0);
    //mp_obj_list_append(mp_sys_path, MP_OBJ_NEW_QSTR(MP_QSTR_)); // current dir (or base dir of the script)
    //mp_obj_list_append(mp_sys_path, MP_OBJ_NEW_QSTR(MP_QSTR__slash_flash));
    //mp_obj_list_append(mp_sys_path, MP_OBJ_NEW_QSTR(MP_QSTR__slash_flash_slash_lib));
    //mp_obj_list_init(mp_sys_argv, 0);
    do_str("print('hello world!', list(x+1 for x in range(10)), end='eol\n')");
    mp_deinit();
    return 0;

//<<<<<<< HEAD
//  chThdSleepMilliseconds(1000);
//=======

  //chThdSleepMilliseconds(1000);
//>>>>>>> 0a4637e205cea9d90df5c1f0cab0337df86d9dff

  while (TRUE) {
/*
    unsigned char b; 
    short int count, i;


  Para eliminar la necesidad de programar el robot cada vez que se energice IFLAB se dispondrá de una región para almacenamiento de programas
  en flash FLASH_PROGRAM_ADDRESS_START (prog_base) FLASH_PROGRAM_ADDRESS_END. El contenido de esta sección es:

  prog_base        : 0xAA = Execute from flasj; 0x55 Read opcodes from serial port
  prog_base +1, +2 : program length High_byte, low_byte
  prog_base +3     : Opcodes list.
  echo -ne '\xAA\x0\x2E\x0\x3\x29\x1\x80\x2\x0\x1\x2\x0\xA\x2\x0\x1\x3\x1A\x1\x81\x2\x0\x1\x2\x0\x5\x2\x0\x1\x3\xB\x55\x1\x0\x24\x10\x56\x1\x0\x24\x10\x4\x8C\x4\x8C\x4\xF\x7' > prog.bin
  echo -ne '\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF' > erase.bin

*/
  }
  return(0);
}


void nlr_jump_fail(void *val) {
    printf("FATAL: uncaught exception %p\n", val);
}


