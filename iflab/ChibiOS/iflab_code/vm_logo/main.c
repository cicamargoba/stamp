#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include <io.h>
#include <machine.h>
#include <board_stamp.h>
#include <vm.h>
#include "efc.h"
#include "flashd.h"

#define BOARD_MCK               48000000
void
run(short int where,int vm_flags)
{
  int ret;

//  chprintf((BaseChannel *)&SD2,"running at %d\n", where);
  CRStack *stack;
  stack = cr_stack_new(256); /* small stack */ 
  ret = vm_run(stack, where, vm_flags);
/*  if (ret == VM_OK)
    chprintf((BaseChannel *)&SD2,"The program finished with status VM_OK\n");
  else if (ret == VM_ERROR)
    chprintf((BaseChannel *)&SD2,"The program finished with status VM_ERROR\n");
  else
    chprintf((BaseChannel *)&SD2,"The program finished with status %d\n", ret);
  chprintf((BaseChannel *)&SD2,"The data stack is%s empty\n", cr_stack_data_isempty(stack) ? "" : " NOT");
  chprintf((BaseChannel *)&SD2,"The code stack is%s empty\n", cr_stack_code_isempty(stack) ? "" : " NOT");
*/  cr_stack_free(stack);
}



//static WORKING_AREA(waThread1, 128);


/*
 * Application entry point.
 */
int main(void) {

  short int pointer = 0;
  int vm_flags = VM_VERBOSE_STACK ;
  halInit();
  chSysInit();

  sdStart(&SD2, NULL);  /* Activates the serial driver 2 */
  sdStart(&SD1, NULL);  /* Activates the serial driver 1 */
  io_init();

  machine_init(vm_flags);
//<<<<<<< HEAD
//  chThdSleepMilliseconds(1000);
//=======

  //debug ("\nHello\n");
  //chThdSleepMilliseconds(1000);
//>>>>>>> 0a4637e205cea9d90df5c1f0cab0337df86d9dff

  while (TRUE) {
    unsigned char b; 
    short int count, i;

/*
  Para eliminar la necesidad de programar el robot cada vez que se energice IFLAB se dispondrá de una región para almacenamiento de programas
  en flash FLASH_PROGRAM_ADDRESS_START (prog_base) FLASH_PROGRAM_ADDRESS_END. El contenido de esta sección es:

  prog_base        : 0xAA = Execute from flasj; 0x55 Read opcodes from serial port
  prog_base +1, +2 : program length High_byte, low_byte
  prog_base +3     : Opcodes list.
  echo -ne '\xAA\x0\x2E\x0\x3\x29\x1\x80\x2\x0\x1\x2\x0\xA\x2\x0\x1\x3\x1A\x1\x81\x2\x0\x1\x2\x0\x5\x2\x0\x1\x3\xB\x55\x1\x0\x24\x10\x56\x1\x0\x24\x10\x4\x8C\x4\x8C\x4\xF\x7' > prog.bin
  echo -ne '\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF' > erase.bin
*/
    unsigned char *flash_data = (unsigned char*)FLASH_PROGRAM_ADDRESS_START;
    if( *flash_data == 0XAA){
      pointer = 0x0000;
      count  = *(++flash_data) << 8;  // High byte
      count |= *(++flash_data);       // Low  byte
      chprintf((BaseChannel *)&SD2,"Flash program length %d bytes\r\n", count);
        while(count--)
        {
          unsigned char d = *(++flash_data);
          write_byte(~d);
          core_byte_set(pointer++, d);
        }
      run(0x0000, vm_flags);
    }

//    chprintf((BaseChannel *)&SD2,"waiting for command \r\n");
    b = read_byte();
//    chprintf((BaseChannel *)&SD2,"command : 0x%x\r\n", b);
    switch (b)
    {
      case 131: /* set-pointer  0x83*/
        pointer = read_byte() << 8;
        pointer |= read_byte();
//        chprintf((BaseChannel *)&SD2,"pointer = %d %xh\r\n", pointer, pointer);
        break;

      case 132: /* read bytes */
//        chprintf((BaseChannel *)&SD2, "[program:] \r\n");
/*        for (i = 0; i < 50; ++i)
          chprintf((BaseChannel *)&SD2, "[%d] %d \r\n", i, core_byte_get(i));
        chprintf((BaseChannel *)&SD2, "[end program]\r\n");*/
        break;

      case 133: /* write-bytes */
        count =  read_byte() << 8;  // High byte
        count |= read_byte();       // Low  byte
//        chprintf((BaseChannel *)&SD2,"will receive %d bytes\r\n", count);
        while(count--)
        {
          unsigned char d = read_byte();
          core_byte_set(pointer++, d);
        }
        break;

      case 134: /* run */
        run(pointer, vm_flags);
        break;

      case 135: /* cricket-check */
        while (read_byte()); /* TODO: Compare name */
        write_byte(55);
        break;
      default :
//        chprintf((BaseChannel *)&SD2, "invalid command %x\n\r");
		break;
    }

  }

  return(0);
}
