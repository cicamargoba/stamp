#include <ch.h>
//#define HAL_USE_PAL 1
//#define HAL_USE_SERIAL 1
#include <hal.h>
//#include <pal.h>

//#include "test.h"
//#include "chprintf.h"
//#include <io.h>
//#include <machine.h>
//#include <board_stamp.h>
#include <weta.h>
//#include "efc.h"
//#include "flashd.h"

PSERIAL serialPorts[2] = { Serial1, Serial2 };

MotorState motors[2] =
{
	{ 0, false, false, false, MOTOR_THIS_WAY, 100 },
	{ 1, false, false, false, MOTOR_THIS_WAY, 100 }
};

ServoState servos[2] =
{
	{ 0, false, 0, { 110, 300, 490, -20 } },
	{ 1, false, 0, { 110, 300, 490, -20 } }
};

GpioPin gpio[9] =
{
	{ (uint32_t)IOPORT1, PIN_LED1, PAL_MODE_OUTPUT_PUSHPULL },
	{ (uint32_t)IOPORT1, PIN_LED2, PAL_MODE_OUTPUT_PUSHPULL },
	{ (uint32_t)IOPORT2, PIN_LED3, PAL_MODE_OUTPUT_PUSHPULL },
	{ (uint32_t)IOPORT2, PIN_LED4, PAL_MODE_OUTPUT_PUSHPULL },
	{ (uint32_t)IOPORT2, PIN_LEDUSER, PAL_MODE_OUTPUT_PUSHPULL },
	{ (uint32_t)IOPORT1, PIN_SWITCH1, PAL_MODE_INPUT_PULLUP },
	{ (uint32_t)IOPORT1, PIN_SWITCH2, PAL_MODE_INPUT_PULLUP },
	{ (uint32_t)IOPORT1, PIN_SWITCH3, PAL_MODE_INPUT_PULLUP },
	{ (uint32_t)IOPORT1, PIN_SWITCH4, PAL_MODE_INPUT_PULLUP }
};

	// Use RAM as program storage for now
//uint8_t fakeFlash[1024];

static Hardware hardware =
{
	(uint8_t*)FLASH_PROGRAM_ADDRESS_START + 1, // Code (assume 0xAA at the start for now)
	{	
		serialPorts,	
		2
	},
	{	
		motors,
		2
	},
	{	
		servos,
		2
	},
	{	
		gpio,
		9
	},
};

/*
 * Application entry point.
 */
int main(void) 
{
	Weta weta;

	hw_init();

	//sdStart(&SD1, NULL);  /* Activates the serial driver 2 */
	//chprintf((BaseChannel *)&SD1,"Starting Weta!\r\n");
	weta_stack_init();
	weta_init(&weta, &hardware, 0);
	weta_debug(&weta, "In main, about to call weta_loop()\r\n");
	weta_loop(&weta);
	
  	return 0;
}
