#include <io.h>
#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"

#define printf(...) chprintf((BaseChannel *)&SD2, __VA_ARGS__)

static char core[4096];

unsigned char
core_byte_get (int pos)
{
  if (pos >= 0 && pos <= 0xfff)
    return core[pos];

  return 0xff;
}

void
core_byte_set(int pos, unsigned char value)
{
  if (pos >= 0 && pos <= 0xfff)
    core[pos] = value;
}

void
io_init()
{
//  cyg_io_lookup( "/dev/haldiag", &handle );
}

void
write_byte(const char b)
{
//  chprintf((BaseChannel *)&SD2, "%c", b);
  chIOPut((BaseChannel *)&SD2, b);
}

unsigned char
read_byte()
{
  unsigned char b;
  b = chIOGet((BaseChannel *)&SD2);
//  write_byte(b); /* hardware echo of the programmer */
  return (uint8_t) b;
}

void
debug (const char *template, ...)
{
  //printf(template);
  chprintf((BaseChannel *)&SD2, template);

}
