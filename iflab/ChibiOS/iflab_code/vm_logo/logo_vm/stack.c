#include <stack.h>
#include <io.h>
#include <stdlib.h>
#include <assert.h>
#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"

struct CRStack
{
  CRStackNode *stack;
  unsigned int size;
  unsigned int data_top;
  unsigned int code_top;
};

CRStack *
cr_stack_new (unsigned int size)
{
  CRStack *s;

  assert(size < 100000); /* Remove this limit if it bothers you */

  s = malloc(sizeof(CRStack));

  if (!s)
  {
//    chprintf((BaseChannel *)&SD2, "malloc failed\r\n");
    return ((CRStack *)EXIT_FAILURE);
  }

  s->size = size;
  s->data_top = 0; /* no data */
  s->code_top = size; /* no calling info */

  s->stack = malloc(sizeof(CRStackNode) * size);

  if (!s->stack)
  {
//    chprintf((BaseChannel *)&SD2, "malloc failed\r\n");
    return ((CRStack *) EXIT_FAILURE);
  }

  return s;
}

void
cr_stack_free (CRStack *s)
{
  assert(s);
  assert(s->stack);

  free(s->stack);
  free(s);
}


int
cr_stack_data_isempty (CRStack *s)
{
  assert(s);

  return s->data_top == 0;
}

int
cr_stack_code_isempty (CRStack *s)
{
  assert(s);

  return s->code_top == s->size;
}

int
cr_stack_isfull (CRStack *s)
{
  assert(s);

  return s->data_top >= s->code_top; /* TODO:  >= or > ? -----  >= (or even ==) is safer */
}

void
cr_stack_data_push (CRStack *s, CRStackNode e)
{
  assert(s);
  assert(!cr_stack_isfull(s));

  s->stack[s->data_top++] = e;
}

CRStackNode
cr_stack_data_pop (CRStack *s)
{
  assert(s);
  assert(!cr_stack_data_isempty(s)); /* stack cannot be empty */

  return s->stack[--s->data_top];
}

CRStackNode
cr_stack_peekaddr (CRStack *s, unsigned int addr)
{
  return s->stack[addr];
}

CRStackNode
cr_stack_data_peek (CRStack *s)
{
  return cr_stack_peekaddr(s, s->data_top - 1);
}

unsigned int
cr_stack_data_topaddress (CRStack *s)
{
  assert(s);
  return s->data_top;
}

void
cr_stack_data_print (CRStack *s)
{
  int i;
  assert(s);

  chprintf((BaseChannel *)&SD2, "(Data Stack(#%d):", s->data_top);

  for (i = s->data_top - 1; i >= 0; --i)
    chprintf((BaseChannel *)&SD2, " %d", s->stack[i]);

  chprintf((BaseChannel *)&SD2, ")\r\n");
}

void
cr_stack_code_push (CRStack *s, CRStackNode e)
{
  assert(s);
  assert(!cr_stack_isfull(s));

  s->stack[--s->code_top] = e;
}

CRStackNode
cr_stack_code_pop (CRStack *s)
{
  assert(s);
  assert(!cr_stack_code_isempty(s)); /* stack cannot be empty */

  return s->stack[s->code_top++];
}

unsigned int
cr_stack_data_popn (CRStack *s, unsigned int n)
{
  assert(s);
  assert(!cr_stack_data_isempty(s)); /* stack cannot be empty */
  s->data_top -= n;
  return (s->data_top);
}


unsigned int
cr_stack_data_pushn (CRStack *s, unsigned int n)
{
  assert(s);
  assert(!cr_stack_data_isempty(s)); /* stack cannot be empty */
  s->data_top = s->data_top + n;
  return (s->data_top);
}

void 
cr_stack_set_data_topaddress  (CRStack *s, unsigned int addr)
{
  assert(s);
  assert(!cr_stack_data_isempty(s)); /* stack cannot be empty */
  s->data_top = addr;   
}


void 
cr_stack_set_data_address  (CRStack *s, unsigned int addr, unsigned int value)
{
  s->stack[addr] = value;   
}


CRStackNode
cr_stack_code_peek (CRStack *s)
{
  assert(s);
  assert(!cr_stack_code_isempty(s)); /* stack cannot be empty */

  return s->stack[s->code_top];
}

void
cr_stack_code_print (CRStack *s)
{
  unsigned int i;
  assert(s);

  chprintf((BaseChannel *)&SD2, "(Code Stack(#%d):", s->code_top);

  for (i = s->code_top; i < s->size; ++i)
    chprintf((BaseChannel *)&SD2, " %d", s->stack[i]);

  chprintf((BaseChannel *)&SD2, ")\r\n");
}
