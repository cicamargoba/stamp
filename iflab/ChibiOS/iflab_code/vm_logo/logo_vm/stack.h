#ifndef __CR__STACK__H__
#define __CR__STACK__H__

typedef short int CRStackNode;

typedef struct CRStack CRStack;

CRStack *    cr_stack_new        (unsigned int size);
void         cr_stack_free       (CRStack *s);

int          cr_stack_data_isempty    (CRStack *s);
int          cr_stack_isfull     (CRStack *s);

void         cr_stack_data_push       (CRStack *s, CRStackNode e);
CRStackNode  cr_stack_data_pop        (CRStack *s);


/* return the top element without removing it */
CRStackNode  cr_stack_data_peek        (CRStack *s);

void         cr_stack_data_print        (CRStack *s);

int          cr_stack_code_isempty    (CRStack *s);
int          cr_stack_isfull     (CRStack *s);

void         cr_stack_code_push       (CRStack *s, CRStackNode e);
CRStackNode  cr_stack_code_pop        (CRStack *s);

/* return the top element without removing it */
CRStackNode  cr_stack_code_peek        (CRStack *s);

/* return the address of the last node */
unsigned int cr_stack_data_topaddress  (CRStack *s);

void cr_stack_set_data_topaddress  (CRStack *s, unsigned int addr);
void cr_stack_set_data_address     (CRStack *s, unsigned int addr, unsigned int value);

unsigned int cr_stack_data_popn (CRStack *s, unsigned int n);
unsigned int cr_stack_data_pushn (CRStack *s, unsigned int n);

/* read the contents of a specific address */
CRStackNode  cr_stack_peekaddr         (CRStack *s, unsigned int addr);

void         cr_stack_code_print        (CRStack *s);

#endif
