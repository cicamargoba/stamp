#ifndef __BOARD_stamp__H__
#define __BOARD_stamp__H__

#define  __inline static inline
#define FLASH_PROGRAM_ADDRESS_START  0x41FC00 
#define FLASH_PROGRAM_ADDRESS_END    0x41FFFF 
#include <motor-interface.h>

#include "ch.h"
#include "hal.h"



void stamp_buzzer_init(void);
void stamp_buzzer_beep(void);

void stamp_buzzer_value(unsigned int val);

void stamp_dcmotor_init(void);

void stamp_control_motor(int idmotor, motor_states state, unsigned char value);

void stamp_servomotor_control(int idmotor, unsigned int value);
void stamp_servomotor_init(void);

void stamp_init_adc(void);

unsigned int stamp_adc(unsigned char channel);

unsigned int stamp_get_gpio(unsigned char digital_input);
void stamp_set_gpio(unsigned char digital_output, unsigned char value);

void read_programm_from_flash(void);
void write_programm_to_flash(void);

/* i2c */
void stamp_i2c_init(void);

void i2c_write (uint8_t slave_address, 
				uint32_t internal_address,
				uint8_t isize, 
				uint8_t *data,
				uint32_t n_bytes);

void i2c_read (	uint8_t slave_address, 
				uint32_t internal_address,
				uint8_t isize, 
				uint8_t  *data,
				uint32_t n_bytes);



#endif
