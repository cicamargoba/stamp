#ifndef __VM__H__
#define __VM__H__

#define VM_ERROR 1
#define VM_OK 2
#define VM_VERBOSE 4
#define VM_VERBOSE_STACK 8

#include <stack.h>

int      vm_run   (CRStack *, unsigned int entry_point, int flags);

/*
 * CALLING CONVENTION


When a call is to be made, for instance with the bytecode -128 3,
we do:

1)  Save the address where the call should return in the code stack.

The function address is 3 in the case of the example (-128 3).

2)  Save the number of parameters (contents of the function address)
    in the data stack.
2)  Save the address of the top of the stack in the code stack.

3) Jump to : funcion address + 1


When you need to do a return:

1) if the function returns a value, pop V from the data stack
2) pop A from the code stack (address of B -- check the next step)
3) Pop B from the data stack (the address of B should be A!)
3) pop B times from the data stack
4) if the funcion returns a value, push V in the data stack
5) pop R from the code stack
6) jump to R
*/
#endif
