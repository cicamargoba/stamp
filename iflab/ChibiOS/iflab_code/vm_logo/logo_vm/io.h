#ifndef __VM__IO__H__
#define __VM__IO__H__

void          io_init        (void);
unsigned char read_byte      (void);
void          write_byte     (const char b);
unsigned char core_byte_get  (int pos);
void          core_byte_set  (int pos, unsigned char value);
void          debug          (const char *template, ...)
                              __attribute__ ((format (printf, 1, 2)));
#endif
