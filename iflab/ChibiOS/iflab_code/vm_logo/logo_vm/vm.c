#include <io.h>
#include <vm.h>
#include <machine.h>
#include <board_stamp.h>
#include <stack.h>
#include <stdlib.h>
#include <assert.h>

#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"


#define FUNC_CALL_FENCE 0x7fff

/* Cricket allows up to 16 global variables */
#define N_GLOBALS 16
static CRStackNode global_vars[16];

unsigned int time;

inline short int
number_pack(unsigned char msb, unsigned char lsb)
{
  return (msb << 8) | lsb;
}

inline unsigned char
lsb(short int x)
{
  return x & 0xff;
}

inline unsigned char
msb(short int x)
{
  return (x >> 8) & 0xff;
}

int
vm_run(CRStack* stack, unsigned int entry_point, int flags)
{
	char op;
	int ip = entry_point;
	int localsLocation = -1;
	int argsLocation   = -1;
	short int func_addr;
	uint8_t data[64];
	uint8_t byte1, byte2, byte3;
	uint32_t int1;


  assert(entry_point < 4096);
  assert(4096 < FUNC_CALL_FENCE); /* You don't want to mess with FUN_CALL_FENCE */
  if (0 == core_byte_get(ip)) /* this should not happen -- it's safe */
    ip++;

#if 0
  {
    int i;
    chprintf((BaseChannel *)&SD2, "[program:] \r\n");
    for (i = 0; i < 50; ++i)
      chprintf((BaseChannel *)&SD2, "[%d] %d \r\n", i, core_byte_get(i));
    chprintf((BaseChannel *)&SD2, "[end program]\r\n");
  }
#endif

vm_process_next_opcode:

  op = core_byte_get(ip++);
  if (flags & VM_VERBOSE_STACK)
  {
//    cr_stack_data_print(stack);
    //chThdSleepMilliseconds(100);
//    cr_stack_code_print(stack);
    //chThdSleepMilliseconds(100);
    //chprintf((BaseChannel *)&SD2, "[%d] %d \r\n", ip, core_byte_get(ip));
    //chThdSleepMilliseconds(100);
    
  }

  switch(op)
  {
    CRStackNode op1, op2, op3, op4, op5, op6, r, who;
    unsigned char firstTime, keepGoing;
    

/*
  Data Stack      ---  unsigned byte (1)  signed int (2)
  Code Stack      ---
*/
    case 1: /* unsigned byte */
    case 2: /* signed int (2 bytes) */
      {
       short int rest = (unsigned char)core_byte_get(ip++);
       if (op == 2)
       {
         rest <<= 8;
         rest |= core_byte_get(ip++);
       }
       cr_stack_data_push(stack, rest);
      }
      break;
/*
  Data Stack      ---  
  Code Stack      ---  block address
*/
    case 3: /* start of list */
      {
        cr_stack_code_push(stack, ip + 1);           /* First instruction of the list */
        ip += 1 + core_byte_get(ip);                 /* jump to block end TODO:   REVISAR 1 + !!!!!*/
      }
      break;

    case 4:                                          /*eob: end of list */
      break;

    case 5:                                          /*eolr: end of list reporter */
      break;

    case 6:                                          /* lthing : get the parameter #arg_n */
      {
        ip++;
      }
      break;
/*
  Data Stack 
  Code Stack  return address, args location    ---
*/
    case 7: /* stop */
      ip = cr_stack_code_pop(stack);
      argsLocation = cr_stack_code_pop(stack);
      if (ip < 0)	// stack has underflowed
        return VM_ERROR;
    break;

/*
  Data Stack return value, argn .. arg2, arg1, nargs  -- return value, argn .. arg2, arg1, nargs
  Code Stack     ---
*/
    case 8: /* output */
      {
      // Space should have been allocated on the stack at the end of the argument list for the return value. (This
      // means that the return value is lower on the stack) If the number of arguments is zero, then the argsLocation
      // place on the stack has actually been taken by the return value placeholder (and the zero it its initial value).
      op1 = cr_stack_data_pop(stack);	                 // op1 = the return value
      op2 = cr_stack_peekaddr(stack, argsLocation);      // op2 = number of args
      // if the number of args is greater than zero, then the numArgs location is real and needs to be stepped past.
      if (op2 > 0)
        op2 = op2 + 1;
      cr_stack_set_data_address(stack, argsLocation - op2, op1);
      }

      break;
/*
  Data Stack times to repeat --- times to repeat
  Code Stack adress block    --- adress block
*/
    case 9:  /* repeat */
      {
        int how_many_left = cr_stack_data_pop(stack);
        CRStackNode jmp_to = cr_stack_code_pop(stack);
        if (how_many_left)
        {
          ip = jmp_to;
          how_many_left--;
          cr_stack_data_push(stack, how_many_left);
          cr_stack_code_push(stack, jmp_to);
        }
      } 
      break;
/*
  Data Stack argument to test --- argument to test
  Code Stack adress block     --- adress block
*/
    case 10: /* if jump */
      {
        op1 = cr_stack_code_pop(stack);
        op2 = cr_stack_data_pop(stack);
        if (op2)
        {
          ip = op1;
          cr_stack_data_push(stack, op2 - 1 );        // make the argument false
          cr_stack_code_push(stack, op1);
        }

      }
      break;
/*
  Data Stack argument to test                 ---  argument to test
  Code Stack then list start, else list start ---  0x0fff, else list start
*/
    case 11: /* ifelse jump */
      {
        op1 = cr_stack_code_pop(stack);            // op1 = ELSE list start
        op2 = cr_stack_code_pop(stack);            // op2 = THEN list start
        op3 = cr_stack_data_pop(stack);            // op3 = argument to test
        if (op2 != 0x0fff)	                   // check for done flag
        {
          if (op3)
          {
            ip = op2;                              // the "THEN" list
            op2 = 0x0fff;	                   // set the done flag
            //cr_stack_data_push(stack, op3);
            cr_stack_code_push(stack, op2);
                                                   // No need to push temp1 here since it will be pushed when ELSE list is
                                                   //	encountered after THEN runs.
          }
          else
          {
            ip = op1;	                           // the "ELSE" list
            op2 = 0x0fff;	                   // set the done flag
            //cr_stack_data_push(stack, op3);
            cr_stack_code_push(stack, op2);
            cr_stack_code_push(stack, op1);
          }
        }
      }
      break;
/*
  Data Stack result of condition  ---  
  Code Stack block address        ---  block address (if condition is true) 
*/
    case 128: /* do (repeat)  while */
      op1 = cr_stack_data_pop(stack);             // result of condition expression
      if (op1)                                    // if true then get the block start address
        ip = cr_stack_code_peek(stack);
      else                                        // else dispose of the block start address
        cr_stack_code_pop(stack); 
      break;
/*
  Data Stack argn .. arg2, arg1, n_args, proc_address  --- arg3, arg2, arg1
  Code Stack argslocation(calling function), ip      --- argslocation (previous), ip (return address)
*/
    case 130: /* op_call  */
      func_addr = cr_stack_data_pop(stack);  // Get function address (proc_address)
      cr_stack_code_push(stack, argsLocation);             // Save the args location used by the calling function
      argsLocation = cr_stack_data_topaddress(stack) - 1;  // Set the args location for use by the called function skipping args counter.
      cr_stack_code_push(stack, ip);                       // Save the current code location for the return
      ip = func_addr + 1;                                    // Now jump to the function
      break;
/*
  Data Stack argn .. arg2, arg1, nargs(argsLocaton), index ---  value
  Code Stack                                               ---
*/
    case 138: /* OP_getparam  */
      op1 = cr_stack_data_pop(stack);                             // Arg index
      op2 = cr_stack_peekaddr( stack, argsLocation - (1 + op1) ); // Index backwards through the stack
      cr_stack_data_push(stack, op2);
    break;
/*
  Data Stack number of local variables  ---  previous localsLocation (for nestead functions), localn (localsLocation) .. local2, localn.
  Code Stack 
*/
    case 154: /* op_enter */                              // Allocate stack space for local variables
      op1 = cr_stack_data_pop(stack);                     // Number of local variables
      cr_stack_data_push(stack, localsLocation);
      localsLocation = cr_stack_data_topaddress(stack);
      cr_stack_data_pushn (stack, op1);                    // stack_top = stack_pop + (number of local variables) 
    break;
/*
  Data Stack index, value  --- 
  Code Stack               ---
*/
    case 134: /* setlocal */
      op1 = cr_stack_data_pop(stack);       // Value at the top
      op2 = cr_stack_data_pop(stack);       // Index of variable next
      if (localsLocation != -1)             // If localsLocation is -1 then we're in global code so assume we're referring to a global variable.
        cr_stack_set_data_address(stack, localsLocation + op2, op1);
      else
	global_vars[op2] = op1;
    break;
/*
  Data Stack index ---  value
  Code Stack       ---
*/
    case 135: /* getlocal */                                
      op1 = cr_stack_data_pop(stack);
      if (localsLocation != -1)
        op2 = cr_stack_peekaddr(stack, localsLocation + op1);
      else                                 // If localsLocation is -1 then we're in global code so assume we're referring to a global variable.
        op2 = global_vars[op1];
      cr_stack_data_push(stack, op2);
      break;
/*
  Data Stack previous localsLocation (for nestead functions), localn (localsLocation) .. local2, localn. --- 
  Code Stack                                                                                             ---
*/
    case 155: /* op_leave */                              // Free space reserved for op_enter
      cr_stack_set_data_topaddress(stack, localsLocation); // TODO: revisar    
      localsLocation = cr_stack_data_pop(stack);          // Restore the caller's locals location (recursive function call)
    break;
/*
  Data Stack index ---  value
  Code Stack       ---
*/
    case 140:  /* count with i from .... */
      op1 = cr_stack_code_pop(stack);            // op1 = Address of top of block
      op2 = cr_stack_data_pop(stack);            // op2 = step 
      op3 = cr_stack_data_pop(stack);            // op3 = to
      op4 = cr_stack_data_pop(stack);            // op4 = from
      op5 = cr_stack_data_pop(stack);            // op5 = index of counter variable   
      firstTime = (op5 & 128) == 128;            // Test msb of counter index to see if this is the first iteration
      if (firstTime)
      {
        op5 &= ~128;
        op6 = op4;
      }
      else 
      {
        if (localsLocation != -1)                 //
          op6 = cr_stack_peekaddr(stack, localsLocation + op5);
	else
          op6 = global_vars[op5];
        op6 += op2;                               // Increment counter
      }
      if (localsLocation != -1)
        cr_stack_set_data_address(stack, localsLocation + op5, op6);
      else
        global_vars[op5] = op6;
      keepGoing = (op2 > 0) ? (op6 <= op3) : (op6 >= op3); // If step > 0 then assume from < to else assume from > to
      if (keepGoing || firstTime)
      {
        ip = op1;                                  // reiterate
        cr_stack_data_push(stack, op5);
        cr_stack_data_push(stack, op4);
        cr_stack_data_push(stack, op3);
        cr_stack_data_push(stack, op2);
        cr_stack_code_push(stack, op1);
      }
      break;
/*
  Data Stack argn .. arg2, arg1, nargs_pop ---
  Code Stack                               ---
*/
    case 153: /* OP_POP       */                          // Clear the top of the stack by the given amount
      op1   = cr_stack_data_pop(stack);                   // Amount to clear (n_args)
      cr_stack_data_popn(stack, op1);                    // Clear stack   TODO: verificar
      break;

    case 12:
      // beep();
      break;
/*
  Data Stack condition     ---
  Code Stack block address ---
*/
    case 14: /* wait until */
      op1 = cr_stack_data_pop(stack);
      if (op1)
      {
        cr_stack_code_pop(stack);
      }
      else
      {
        ip = cr_stack_code_peek(stack);
      }
      break;
/*
  Data Stack               ---
  Code Stack block address ---
*/
    case 15:  /* loop */
      {
        CRStackNode jmp_to = cr_stack_code_pop(stack);
        ip = jmp_to;
        cr_stack_code_push(stack, jmp_to); /* save returning address */
      }
      break;
/*
  Data Stack delay  ---
  Code Stack        ---
*/
    case 16: /* wait */
      op1 = cr_stack_data_pop(stack);
      machine_wait(op1);
      break;
      
      
    case 17:
      r = chTimeNow() - time;
      cr_stack_data_push(stack, r);
      break;         
    
    case 18:        
      time = chTimeNow();
      break;
    /*
     * TODO:
     * 17 - timer
     * 18 - resett
     */
/*
  Data Stack data to send  ---
  Code Stack               ---
*/
    case 19: /* send */
        send_serial_data( (cr_stack_data_pop(stack)) & 0x00FF );
      break;
/*
  Data Stack        ---  new data
  Code Stack        ---
*/
    case 20: /* ir */
        cr_stack_data_push(stack , read_serial_data(0));
      break;
/*
  Data Stack        ---  data available
  Code Stack        ---
*/
    case 21: /*  new ir */
        cr_stack_data_push(stack, is_data_read(0));
      break;
/*
  Data Stack        ---  randon number
  Code Stack        ---
*/
   case 22: /* random */
       cr_stack_data_push(stack, (int) (32768.0 * (rand() / (RAND_MAX + 1.0))));
     break;
/*
  Data Stack delay op1 op2  ---  result
  Code Stack                ---
*/
    case 23: /* + */
    case 24: /* - */
    case 25: /* * */
    case 26: /* / */
    case 27: /* % */
    case 28: /* == */
    case 29: /* > */
    case 30: /* < */
    case 31: /* bitwise AND */
    case 32: /* bitwise OR */
    case 33: /* bitwise XOR */
    case 131:/* <= */
    case 132:/* >= */
    case 133:/* != */
      op2 = cr_stack_data_pop(stack); 
      op1 = cr_stack_data_pop(stack);
      switch(op)
      {
        case 23: r = op1 + op2; break;
        case 24: r = op1 - op2; break;
        case 25: r = op1 * op2; break;
        case 26:
          if (!op2) /* division by zero allowed */
            r = 0;
          else
            r = op1 / op2;
          break;
        case 27:
          if (!op2) /* modulo by zero allowed */
            r = 0;
          else
            r = op1 % op2;
          break;
        case 28:  r = op1 == op2; break;
        case 29:  r = op1 >  op2; break;
        case 30:  r = op1 <  op2; break;
        case 31:  r = op1 &  op2; break;
        case 32:  r = op1 |  op2; break;
        case 33:  r = op1 ^  op2; break;
        case 131: r = op1 <= op2; break;
        case 132: r = op1 >= op2; break;
        case 133: r = op1 != op2; break;  // ojo en babuino == y !=  tienen el mismo opcode
        default:
          break;
      }
      cr_stack_data_push(stack, r);
      break;
/*
  Data Stack op1   ---  not(op1)
  Code Stack       ---
*/
    case 34: /* not */
      op1 = cr_stack_data_pop(stack);
      r =  0x1 & (op1 ^ 0x1);
      cr_stack_data_push(stack, r);
      break;
/*
  Data Stack value index  ---  result
  Code Stack              ---
*/
    case 35: /* setglobal */
      op1 = cr_stack_data_pop(stack);
      who = cr_stack_data_pop(stack);
      assert(who >=0 && who <= 15);
      global_vars[who] = op1;
      break;
/*
  Data Stack index  ---  value
  Code Stack        ---
*/
    case 36: /* getglobal */
      who = cr_stack_data_pop(stack);
      assert(who >=0 && who <= 15);
      r = global_vars[who];
      cr_stack_data_push(stack, r);
      break;
/*
  Data Stack   ---  TODO
  Code Stack   ---
*/
    case 37: /* aset */
      op1 = cr_stack_data_pop(stack); /* value */
      op2 = cr_stack_data_pop(stack); /* index */
      who = cr_stack_data_pop(stack); /* base */
      core_byte_set(who + op2 * 2, msb(op1));
      core_byte_set(who + op2 * 2 + 1, lsb(op1));
      break;
/*
  Data Stack   ---  TODO
  Code Stack   ---
*/
    case 38: /* aget */
      op2 = cr_stack_data_pop(stack); /* index */
      who = cr_stack_data_pop(stack); /* base */
      r = who + op2 * 2; /* pointer to MSB */
      r = number_pack(core_byte_get(r), core_byte_get(r + 1));
      cr_stack_data_push(stack, r);
      break;
/*
  Data Stack motor   ---  
  Code Stack         ---
*/
    case 90: 
      op1 = cr_stack_data_pop(stack);
      motor_select(op1);
    break;
/*
  Data Stack delay  ---  
  Code Stack        ---
*/
    case 50: /* onfor */
      op1 = cr_stack_data_pop(stack);
      motor_turn_on();
      machine_wait(op1);
      motor_turn_off();
      break;
    case 46:  motor_select(0x1); break; /* select motor a */
    case 47:  motor_select(0x2); break; /* select motor b */
    case 48:  motor_select(0x3); break; /* select a, b */
    case 63:  motor_select(0x4); break; /* select motor c */
    case 64:  motor_select(0x8); break; /* select motor d */
    case 65:  motor_select(0xc); break; /* select motor c,d */
    case 66:  motor_select(0xf); break; /* select motor a,b,c,d */
    case 49: /* on */
      motor_turn_on();
      break;
    case 51: /* off */
      motor_turn_off();
      break;
    case 52: /* thisway */
      motor_thisway_thatway(MOTOR_THIS_WAY);
      break;
    case 53: /* thatway */
      motor_thisway_thatway(MOTOR_THAT_WAY);
      break;
    case 54: /* rd: reverse direction */
      motor_reverse_direction();
      break;
/*
  Data Stack speed  ---
  Code Stack        ---
*/
    case 59: /* setpower */
      motor_set_speed(cr_stack_data_pop(stack));
      break;
    case 60: /* brake */
      motor_set_brake(); /* active braking */
      break;
/*
  Data Stack sensor_number  ---  sensor value
  Code Stack                ---
*/
    case 55: /* sensora */
    case 56: /* sensorb */
      who = op == 55 ? 1 : 2;
      r = machine_sensor_get(who);
      cr_stack_data_push(stack, r);
      break;
    case 73: /* sensor3 */
    case 74: /* sensor4 */
      who = op == 73 ? 3 : 4;
      r = machine_sensor_get(who);
      cr_stack_data_push(stack, r);
      break;
/*
  Data Stack switch_number  ---  switch value
  Code Stack                ---
*/
    case 57: /* switcha */
    case 58: /* switchb */
      who = op == 57 ? 1 : 2;
      r = machine_switch_get(who);
      cr_stack_data_push(stack, (r & 0x01));
      break;
    case 79: /* switch3 */
    case 80: /* switch4 */
      who = op == 79 ? 3 : 4;
      r = machine_switch_get(who);
      cr_stack_data_push(stack, (r & 0x01));
      break;
      break;
/*
  Data Stack switch_number  ---  TODO
  Code Stack                ---  TODO
*/
    case 61: /* bsend */
//       bus_port_send(cr_stack_data_pop(stack));
      break;

    case 62: /* bsr */
//       cr_stack_data_push(stack, bus_port_send_report_reply(cr_stack_data_pop(stack)));
      break;
   case 68: /* stop */
     return VM_OK;
     break;
    case 85: /* LEDON */
    case 86: /* LEDOFF */
      who = op == 85 ? 1 : 0;
      stamp_set_gpio(5, who); 
        
      break;
/*
  Data Stack position  ---
  Code Stack           ---
*/
//    case 87:  /* servo heading */
    case 88: /* servo right */
    case 89: /* servo left */
      who = op == 88 ? 1 : 2;
      op1 = cr_stack_data_pop(stack);
      servo_set_position(who, op1);
      break;

	case 93: /* i2c write */
		byte1 = cr_stack_data_pop(stack);	// internal addr. size
     	int1 = cr_stack_data_pop(stack);	// internal address
		byte2 = cr_stack_data_pop(stack);	// value
		byte3 = cr_stack_data_pop(stack);	// slave address
		
//		chprintf((BaseChannel *)&SD2, "\n\rsl addr: %d, i addr: %d, value: %d, isize: %d, p4: %d\n\r\n\r", byte3, int1, byte2, byte1);		
		/* writing at24c */
		i2c_write(byte3, int1, byte1, &byte2, 1);
		break;
	case 94: /* i2c read */
		memset(data, 0, 64);
		byte1 = cr_stack_data_pop(stack);	// internal addr. size
		int1 = cr_stack_data_pop(stack);	// internal address
		byte3 = cr_stack_data_pop(stack);	// slave address
   		
		/* reading AT24C */		
		//i2c_read(byte1, 0x0000, 2, data, 1);
		
		/* reading tmp102 (without address). byte1 is the slave address */
		i2c_read(byte3, int1, byte1, data, 2);
		//chprintf((BaseChannel *)&SD2, "\n\r array data:%d, %d  \n\r", data[0], data[1]);		

		cr_stack_data_push(stack, data[0]);
		break;
   
// /* Second serial Interface */    
    case 142:
      op2 = cr_stack_data_pop(stack);
      op1 = cr_stack_data_pop(stack);
      send_serial_iflab( op2 & 0x00FF , op1-1 );
      break;
    case 143:
      op1 = cr_stack_data_pop(stack);
      cr_stack_data_push(stack , read_serial_data(op1-1));      
      break;
    case 144:
      op1 = cr_stack_data_pop(stack);
      cr_stack_data_push(stack, is_data_read(op1-1));
      break;

// /* Digital Output */
    case 151:
      op2 = cr_stack_data_pop(stack);
      op1 = cr_stack_data_pop(stack);
      stamp_set_gpio(op2+1,op1);
      break;

// /* Digital Input */
    case 150:
      op1 = cr_stack_data_pop(stack);
      cr_stack_data_push(stack, (stamp_get_gpio(op1+1) & 0x01) );
      break;

// /* Analog Input */
    case 148:
      who = cr_stack_data_pop(stack);
      r = machine_sensor_get(who);
      cr_stack_data_push(stack, r);
      break;  
      


    default:
      break;
  }



  goto vm_process_next_opcode;
}
