#ifndef __WETA_DEFS_H__
#define __WETA_DEFS_H__

#define WETA_STACK_SIZE 256
#define WETA_MAX_STACKS 1		// ie only 1 VM at a time

#define SUPPORT_32BIT
#define SUPPORT_FLOAT
#define SUPPORT_DOUBLE
#define SUPPORT_STRING

#define DEFAULT_FORMAT_BYTE  "%02x"
#define DEFAULT_FORMAT_INT   "%d"
#define DEFAULT_FORMAT_FLOAT "%g"

#endif //__WETA_DEFS_H__