#ifndef __HW_BUZZER_H__
#define __HW_BUZZER_H__

#include <stdint.h>

extern void hw_buzzer_init(uint16_t flags);
extern void hw_buzzer_beep(void);


#endif // __HW_BUZZER_H__