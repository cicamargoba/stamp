#ifndef __HW_SERIAL_H__
#define  __HW_SERIAL_H__

#include <stdint.h>
#include <stdbool.h>
#include <ch.h>


#define Serial1 (&SD1)
#define Serial2 (&SD2)

struct SerialDriver;

typedef struct SerialDriver SERIAL, *PSERIAL;

typedef struct
{
	PSERIAL*  ports;
	uint8_t n_ports;
} SerialPorts;

extern void    hw_serial_init(uint16_t flags);
extern void    hw_serial_start(PSERIAL port);
extern void    hw_serial_start_all(SerialPorts* ports);
extern uint8_t hw_serial_read(PSERIAL port, uint8_t* buf, uint8_t length);
extern uint8_t hw_serial_read_byte(PSERIAL port);
extern uint8_t hw_serial_write(PSERIAL port, uint8_t* buf, uint8_t length);
extern void    hw_serial_write_byte(PSERIAL port, uint8_t value);
extern bool    hw_serial_available(PSERIAL port);

#endif // __HW_SERIAL_H__