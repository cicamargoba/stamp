#ifndef __HW_H__
#define  __HW_H__

#include "hw_endian.h"
#include "hw_serial.h"
#include "hw_motor.h"
#include "hw_servo.h"
#include "hw_gpio.h"
#include "hw_adc.h"
#include "hw_dac.h"
#include "hw_i2c.h"
#include "hw_buzzer.h"
#include "hw_time.h"

typedef struct _Hardware
{
	uint8_t*    code;
	SerialPorts	sports;
	Motors		motors;
	Servos      servos;
	Gpio		gpio;
} Hardware;

extern void hw_init(void);
extern void hw_init_specific(Hardware* hardware, uint16_t flags);

#endif // __HW_H__
