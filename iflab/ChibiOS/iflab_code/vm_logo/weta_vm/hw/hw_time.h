#ifndef __HW_TIME_H__
#define __HW_TIME_H__

#include <stdint.h>

extern void     hw_time_waitms(uint16_t ms);
extern uint32_t hw_time_now(void);

#endif // __HW_TIME_H__