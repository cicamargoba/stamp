#ifndef __HW_ADC_H__
#define __HW_ADC_H__

#include <stdint.h>
#include <stdbool.h>

extern void hw_adc_init(uint16_t flags);
extern bool hw_adc_get(uint8_t channel, int16_t* value);

#endif // __HW_ADC_H__