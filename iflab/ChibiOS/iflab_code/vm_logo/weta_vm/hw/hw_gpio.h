#ifndef __HW_GPIO_H__
#define __HW_GPIO_H__

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
	uint32_t port;
	uint32_t pin;
	uint32_t mode;
} GpioPin;

typedef struct
{
	GpioPin* pins;
	uint8_t  n_pins;
} Gpio;

extern void hw_gpio_init(Gpio* gpio, uint16_t flags);
extern bool hw_gpio_get(Gpio* gpio, uint8_t i, bool* value);
extern bool hw_gpio_set(Gpio* gpio, uint8_t i, bool value);

#endif // __HW_GPIO_H__