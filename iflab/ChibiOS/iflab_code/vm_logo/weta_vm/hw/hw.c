#include "hw.h"
#include "ch.h"
#include "hal.h"


void 
hw_init(void)
{
	halInit();
	chSysInit();
}

void 
hw_init_specific(Hardware* hardware, uint16_t flags)
{
	hw_serial_init(flags);
	hw_serial_start_all(&hardware->sports);
	hw_gpio_init(&hardware->gpio, flags);
	hw_adc_init(flags);
	hw_dac_init(flags);
	hw_motor_init(flags);
	hw_servo_init(flags);
	hw_i2c_init(flags);
	hw_buzzer_init(flags);
}