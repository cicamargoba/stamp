/**
 * @file weta_flash.c
 * @author Murray Lang
 * @date 05/15/15
 * @brief Implementation of flash storage for byte codes
 */
#include "weta_store.h"
#include "./hw/hw_endian.h"

//#include <ch.h>				// For debugging
//#include <hal.h>			// For debugging
//#include "./hw/hw_serial.h" // For debugging
//#include <stdio.h>          // For debugging (sprintf())

#include <stdlib.h>	// for malloc() and free()

struct _STORE
{
	uint8_t*    startAddress;
	uint8_t* 	data;
	WetaCodePtr length;
};

//*******************************************************************************
PSTORE 
weta_store_open_read(uint8_t* flashAddr)
{
	PSTORE store = (PSTORE)malloc(sizeof(STORE));

	store->startAddress = flashAddr;
		// Read the data length from the first bytes
	store->length       = (WetaCodePtr)ntoh_uint16(flashAddr);
	store->data         = malloc(store->length + sizeof(uint16_t));

		// Read ALL of the data into the cache!
		// This assumes that the compiler does the work involved in reading
		// from the flash memory space.
	uint8_t* flashCodes = flashAddr;
	WetaCodePtr index;
	for (index = 0; index < (store->length + sizeof(uint16_t)); index++)
	{
		//char szMsg[32];
		//sprintf(szMsg,"store->data[%d] = %d\r\n", index, *flashCodes);
		//hw_serial_write(Serial2, (uint8_t*)szMsg, strlen(szMsg));
		store->data[index] = *flashCodes++;
		
	}
	
	return store;
}

//*******************************************************************************
PSTORE 
weta_store_open_write(uint8_t* flashAddr, WetaCodePtr length)
{
	PSTORE store = (PSTORE)malloc(sizeof(STORE));

	store->startAddress = flashAddr;
	store->length       = length;
		// Allocate enough for the data plus the length value
	store->data         = malloc(store->length + sizeof(uint16_t));
		// Convert the length to network byte order and store at the front
	hton_uint16((uint16_t)length, store->data);
	
	return store;
}

//*******************************************************************************
WetaCodePtr 
weta_store_flush(PSTORE store)
{
		// Assume that flash is in fact ram for now
	WetaCodePtr i;
	for (i = 0; i < (store->length + sizeof(uint16_t)); i++)
		store->startAddress[i] = store->data[i];	
	return i; 
}

//*******************************************************************************
void        
weta_store_close(PSTORE store)
{
	free(store->data);	// Free the data buffer first
	free(store);
}

//*******************************************************************************
WetaCodePtr weta_store_write_byte(
	PSTORE      store, 
	WetaCodePtr address,  
	uint8_t     val
	)
{
	store->data[address + sizeof(uint16_t)] = val;
	
	return address + 1;
}

//*******************************************************************************
WetaCodePtr weta_store_write_bytes(
	PSTORE      store, 
	WetaCodePtr address, 
	uint8_t*    buf, 
	WetaCodePtr length
	)
{
	uint8_t* from    = buf; 
	uint8_t* to      = store->data + address + sizeof(uint16_t);
	uint8_t i;
	for (i = 0; i < length; i++)
		*to++ = *from++;
		
	return address + length;
}

//*******************************************************************************
WetaCodePtr weta_store_write_uint16(
	PSTORE      store, 
	WetaCodePtr address, 
	uint16_t    val)
{
	uint8_t  buf[sizeof(val)];
	hton_uint16(val, buf);
	return weta_store_write_bytes(store, address, buf, sizeof(buf));
}

//*******************************************************************************
WetaCodePtr weta_store_write_uint32(
	PSTORE      store, 
	WetaCodePtr address, 
	uint32_t    val)
{
	uint8_t  buf[sizeof(val)];
	hton_uint32(val, buf);
	return weta_store_write_bytes(store, address, buf, sizeof(buf));
}

//*******************************************************************************
WetaCodePtr weta_store_write_float(
	PSTORE      store, 
	WetaCodePtr     address,  
	float           val)
{
	uint8_t  buf[sizeof(val)];
	hton_float(val, buf);
	return weta_store_write_bytes(store, address, buf, sizeof(buf));
}

//*******************************************************************************
WetaCodePtr weta_store_write_double(
	PSTORE      store, 
	WetaCodePtr     address, 
	double          val)
{
	uint8_t  buf[sizeof(val)];
	hton_double(val, buf);
	return weta_store_write_bytes(store, address, buf, sizeof(buf));
}

//*******************************************************************************
WetaCodePtr weta_store_read_byte(
	PSTORE      store, 
	WetaCodePtr     address,  
	uint8_t*        pval)
{
	*pval = store->data[address + sizeof(uint16_t)];
	
	return address + 1;
}

//*******************************************************************************
WetaCodePtr weta_store_read_bytes(
	PSTORE      store, 
	WetaCodePtr     address, 
	uint8_t*        buf, 
	WetaCodePtr     length)
{
	uint8_t* to = buf;
	uint8_t* from = store->data + address + sizeof(uint16_t);
	uint8_t i;
	for (i = 0; i < length; i++)
		*to++ = *from++;
	return address + length;
}

//*******************************************************************************
WetaCodePtr weta_store_read_uint16(PSTORE store, WetaCodePtr address, uint16_t* pval)
{
	uint8_t  buf[sizeof(*pval)];
	WetaCodePtr addr = weta_store_read_bytes(store, address, buf, sizeof(buf));
	if (addr != INVALID_CODEPTR)
		*pval = ntoh_uint16(buf);

	return addr;
}

//*******************************************************************************
WetaCodePtr weta_store_read_uint32(PSTORE store, WetaCodePtr address, uint32_t* pval)
{
	uint8_t  buf[sizeof(*pval)];
	WetaCodePtr addr = weta_store_read_bytes(store, address, buf, sizeof(buf));
	if (addr != INVALID_CODEPTR)
		*pval = ntoh_uint32(buf);

	return addr;
}

//*******************************************************************************
WetaCodePtr weta_store_read_float(PSTORE store, WetaCodePtr address,  float* pval)
{
	uint8_t  buf[sizeof(*pval)];
	WetaCodePtr addr = weta_store_read_bytes(store, address, buf, sizeof(buf));
	if (addr != INVALID_CODEPTR)
		*pval = ntoh_float(buf);

	return addr;
}

//*******************************************************************************
WetaCodePtr weta_store_read_double(PSTORE store, WetaCodePtr address, double* pval)
{
	uint8_t  buf[sizeof(*pval)];
	WetaCodePtr addr = weta_store_read_bytes(store, address, buf, sizeof(buf));
	if (addr != INVALID_CODEPTR)
		*pval = ntoh_double(buf);

	return addr;
}
