#ifndef __WVMCODES_H__
#define __WVMCODES_H__

#include "WvmBaseCodes.h"
#include "WvmCommCodes.h"
#include "WvmMathCodes.h"
#include "WvmIoCodes.h"

#endif // __WVMCODES_H__
