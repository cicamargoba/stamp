#ifndef __WETA_PLATFORM_H__
#define __WETA_PLATFORM_H__

#include <stdint.h>
#include <stdbool.h>
/**
 * @brief The type of a byte code address.
 */
typedef uint16_t WetaCodePtr;
typedef int16_t  WetaStackPtr;

/**
 * @brief define the platform-specific function for reading stack pointers. 
 */
#define weta_store_read_stackptr(sto,i,val)  weta_store_read_uint16(sto, (i), (uint16_t*)(val))

/**
 * @brief define the platform-specific function for reading code pointers. 
 */
#define weta_store_read_codePtr weta_store_read_uint16

#define weta_stack_pushCodePtr weta_stack_pushUint16
#define weta_stack_popCodePtr  weta_stack_popUint16
#define weta_stack_topCodePtr  weta_stack_topUint16
#define weta_stack_setCodePtr  weta_stack_setUint16
#define weta_stack_getCodePtr  weta_stack_getUint16

#define FLASH_PROGRAM_ADDRESS_START  0x41FC00 
#define FLASH_PROGRAM_ADDRESS_END    0x41FFFF

#define PIN_SWITCH1		6
#define PIN_SWITCH2		7
#define PIN_SWITCH3		8
#define PIN_SWITCH4		11

#define PIN_LED1		12
#define PIN_LED2		15
#define PIN_LED3		1
#define PIN_LED4		0
#define PIN_LEDUSER		11

#define LED1		0
#define LED2		1
#define LED3		2
#define LED4		3
#define LEDUSER		4	
#define SWITCH1		5
#define SWITCH2		6
#define SWITCH3		7
#define SWITCH4		8	   

#define SUPPORT_32BIT
#define SUPPORT_DOUBLE
#define SUPPORT_FLOAT
#define SUPPORT_STRING

#define FORCE_STACK_BYTE_ALIGNED
 
 
#endif //__WETA_PLATFORM_H__
