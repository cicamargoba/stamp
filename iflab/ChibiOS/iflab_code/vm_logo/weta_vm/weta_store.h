#ifndef __WETA_FLASH_H__
#define __WETA_FLASH_H__
/** 
 * @file weta_flash.h
 * @author Murray Lang
 * @brief Functions for accessing byte codes in flash memory.
 */

#include "weta_platform.h"

struct _STORE;

typedef struct _STORE STORE, *PSTORE;

#define INVALID_CODEPTR ((WetaCodePtr)~0)

/**	
 * @brief Open flash storage for writing.
 * 
 * Creates a handle that can only be used for write operations. Write operations
 * will be cached, and not actually written to flash until @ref weta_store_flush()
 * is called.
 * 
 * @param flashAddr Start address of flash memory to write to
 * @param length Maximum length of data to be written
 * @return Handle to be used in subsequent operations (@c NULL if failed)
 */
extern	PSTORE weta_store_open_write(uint8_t* flashAddr, WetaCodePtr length);

/**
 * @brief Open flash storage for reading.
 * 
 * Creates a handle that can only be used for read operations.
 * 
 * @param flashAddrc Start address of flash memory to read
 * @return Handle to be used in subsequent write operations (@c NULL if failed)
 */
extern	PSTORE weta_store_open_read(uint8_t* flashAddr);

/**
 * @brief Write previously cached data to flash memory.
 * @param hStore
 * @return Offset of flash memory following flushed data (@c INVALID_CODEPTR if failed)
 */
extern	WetaCodePtr weta_store_flush(PSTORE store);

/**
 * @brief Release resources used for this handle.
 * @param hStore
 */
extern	void weta_store_close(PSTORE store);

/**
 * @brief Write a single byte. 
 * @param hStore Handle returned by @ref weta_store_open_write()
 * @param Address Offset from the flash start address at which to write the byte.
 * @param val Byte value to write
 * @return Offset to the data immediately following the byte written
 */
extern	WetaCodePtr weta_store_write_byte(PSTORE store, WetaCodePtr address,  uint8_t val);

/**
 * @brief Write the given bytes.
 * @param hStore Handle returned by @ref weta_store_open_write()
 * @param address Offset from the flash start address at which to write the bytes.
 * @param buf Array of bytes to write
 * @param length Number of bytes to write
 * @return Offset to the data immediately following the data written
 */
extern	WetaCodePtr weta_store_write_bytes(PSTORE store, WetaCodePtr address, uint8_t* buf, WetaCodePtr length);

/**
 * @brief Write a uint16_t value in network byte order.
 * @param hStore Handle returned by @ref weta_store_open_write()
 * @param address address Offset from the flash start address at which to write the value.
 * @param val Value to write
 * @return Offset to the data immediately following the data written
 */
extern	WetaCodePtr weta_store_write_uint16(PSTORE store, WetaCodePtr address, uint16_t val);

/**
 * @brief Write a uint32_t value in network byte order.
 * @param hStore Handle returned by @ref weta_store_open_write()
 * @param address address Offset from the flash start address at which to write the value.
 * @param val Value to write
 * @return Offset to the data immediately following the data written
 */
extern	WetaCodePtr weta_store_write_uint32(PSTORE store, WetaCodePtr address, uint32_t val);

/**
 * @brief Write a float value in network byte order.
 * @param hStore Handle returned by @ref weta_store_open_write()
 * @param address address Offset from the flash start address at which to write the value.
 * @param val Value to write
 * @return Offset to the data immediately following the data written
 */
extern	WetaCodePtr weta_store_write_float(PSTORE store, WetaCodePtr address,  float val);

/**
 * @brief Write a double value in network byte order.
 * @param hStore Handle returned by @ref weta_store_open_write()
 * @param address address Offset from the flash start address at which to write the value.
 * @param val Value to write
 * @return Offset to the data immediately following the data written
 */
extern	WetaCodePtr weta_store_write_double(PSTORE store, WetaCodePtr address, double val);

/**
 * @brief Read a single byte. 
 * @param hStore Handle returned by @ref weta_store_open_read()
 * @param Address Offset from the flash start address from which to read the byte.
 * @param pval Byte value to write
 * @return Offset to the data immediately following the byte written
 */
extern	WetaCodePtr weta_store_read_byte(PSTORE store, WetaCodePtr address,  uint8_t*pval);

/**
 * @brief Read the given number of bytes.
 * @param hStore Handle returned by @ref weta_store_open_read()
 * @param address Offset from the flash start address from which to read the bytes.
 * @param buf Buffer to place the bytes read
 * @param length Number of bytes to read
 * @return Offset to the data immediately following the data read
 */
extern	WetaCodePtr weta_store_read_bytes(PSTORE store, WetaCodePtr address, uint8_t* buf, WetaCodePtr length); 

/**
 * @brief Read a uint16_t value (stored in network byte order) and convert it to host byte order.
 * @param hStore Handle returned by @ref weta_store_open_read()
 * @param address address Offset from the flash start address fromt which to read the value.
 * @param pval Value read from flash
 * @return Offset to the data immediately following the data read
 */
extern	WetaCodePtr weta_store_read_uint16(PSTORE store, WetaCodePtr address, uint16_t* pval);

/**
 * @brief Read a uint32_t value (stored in network byte order) and convert it to host byte order.
 * @param hStore Handle returned by @ref weta_store_open_read()
 * @param address address Offset from the flash start address fromt which to read the value.
 * @param pval Value read from flash
 * @return Offset to the data immediately following the data read
 */
extern	WetaCodePtr weta_store_read_uint32(PSTORE store, WetaCodePtr address, uint32_t* pval);

/**
 * @brief Read a float value (stored in network byte order) and convert it to host byte order.
 * @param hStore Handle returned by @ref weta_store_open_read()
 * @param address address Offset from the flash start address fromt which to read the value.
 * @param pval Value read from flash
 * @return Offset to the data immediately following the data read
 */
extern	WetaCodePtr weta_store_read_float(PSTORE store, WetaCodePtr address,  float* pval);

/**
 * @brief Read a double value (stored in network byte order) and convert it to host byte order.
 * @param hStore Handle returned by @ref weta_store_open_read()
 * @param address address Offset from the flash start address fromt which to read the value.
 * @param pval Value read from flash
 * @return Offset to the data immediately following the data read
 */
extern	WetaCodePtr weta_store_read_double(PSTORE store, WetaCodePtr address, double* pval);

#endif //__WETA_FLASH_H__
