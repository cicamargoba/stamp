/**
 * @file weta.c
 * @brief Main implementation file of a Weta VM
 */
#include "weta.h"
#include "weta_defs.h"
#include "weta_stack.h"
#include "weta_store.h"
#include "WvmCodes.h"
#include <stdlib.h>
#include <stdio.h>

#ifdef SUPPORT_STRING
#include <string.h>
#endif

#if defined(SUPPORT_FLOAT) || defined(SUPPORT_DOUBLE)
#include <math.h>
#endif

//#include "machine.h"
//#include "board_stamp.h"

//#include <ch.h>
#include <stdlib.h>

static char* pszByteFormat  = DEFAULT_FORMAT_BYTE;
static char* pszIntFormat   = DEFAULT_FORMAT_INT;
static char* pszFloatFormat = DEFAULT_FORMAT_FLOAT;

// Functions implemented later in this file

void init_registers(Registers* regs);
void init_states(WetaStates* states);
void comm_loop(Weta* pWeta);
void exec_loop(Weta* pWeta);
WetaStackPtr getArgsLocation(Registers* pRegs);
void descendBlock(Registers* pRegs);
void ascendBlock(Registers* pRegs);
void setBlockExecuted(Registers* pRegs);
bool hasBlockExecuted(Registers* pRegs);
void pushRegisters(Weta* pWeta);
void popRegisters(Weta* pWeta);

bool with_current_type(Weta* pWeta);
bool with_uint8(Weta* pWeta);
bool with_int8(Weta* pWeta);
bool with_uint16(Weta* pWeta);
bool with_int16(Weta* pWeta);
bool with_bool(Weta* pWeta);
bool with_stackptr(Weta* pWeta);
#ifdef SUPPORT_32BIT
bool with_uint32(Weta* pWeta);
bool with_int32(Weta* pWeta);
#endif
#ifdef SUPPORT_FLOAT
bool with_float(Weta* pWeta);
#endif 
#ifdef SUPPORT_DOUBLE
bool with_double(Weta* pWeta);
#endif
#ifdef SUPPORT_STRING
bool with_string(Weta* pWeta);
#endif

//*******************************************************************************
void
weta_init(Weta* pWeta, Hardware* pHardware, uint16_t flags)
{
	hw_init_specific(pHardware, flags);
	
	pWeta->hal = pHardware;
	pWeta->startAddress = 0;
	pWeta->sport = pHardware->sports.ports[0];
	pWeta->debugsport = pHardware->sports.ports[1];
	
	weta_debug(pWeta, "weta_init()\r\n");
	
	weta_reset(pWeta);
}

void 
weta_reset(Weta* pWeta)
{
	init_registers(&pWeta->regs);
	init_states(&pWeta->states);
	
	if (pWeta->stack)
		weta_stack_release(pWeta->stack);
	pWeta->stack = weta_stack_allocate(/*WETA_STACK_SIZE*/);
	
	pWeta->formats.pszByteFormat  = pszByteFormat;
	pWeta->formats.pszIntFormat   = pszIntFormat;
	pWeta->formats.pszFloatFormat = pszFloatFormat;
}

//*******************************************************************************
void 
weta_debug(Weta* pWeta, char * msg)
{
	hw_serial_write(pWeta->debugsport, (uint8_t*)msg, strlen(msg));
}

//*******************************************************************************
void 
weta_loop(Weta* pWeta)
{
	pWeta->states.runRequest = RUNNING; // Spoof press of 'run' button
	bool abort = false;
	
	while (!abort)
	{
	//debounce();
	
	switch (pWeta->states.machineState)
		{
		case READY:
			if (hw_serial_available(pWeta->sport))
			{
				pWeta->states.machineState = COMM;
				weta_debug(pWeta, "Weta state change READY->COMM\r\n");
			}
			else if (pWeta->states.runRequest == RUNNING)
			{
				//Serial.println("setMachineState(RUN)");
				pWeta->states.machineState = RUN;
				weta_debug(pWeta, "Weta state change READY->RUN\r\n");
			}
			else
			{
				//weta_debug(pWeta, "Weta machine state:READY\r\n");
			}
			break;

		case COMM:
			comm_loop(pWeta);
			pWeta->states.machineState = READY;
			break;

		case RUN:
			init_registers(&pWeta->regs);
			weta_stack_initStack(pWeta->stack);
			//weta_debug(pWeta, "About to call exec_loop()\r\n");
			exec_loop(pWeta);
			//_motors.off();
			pWeta->states.machineState = READY;
			pWeta->states.runRequest = STOPPED;
			break;
		}
	}
}

//*******************************************************************************
void comm_loop(Weta* pWeta)
{
	uint8_t		cmd = 0;
	uint8_t		remainingChars = 0;
	WetaCodePtr start_address = 0;
	WetaCodePtr	byte_count = 0;
	bool        byte_count_written = false;
	
	//weta_debug(pWeta, "comm_loop()\r\n");

	pWeta->states.waitingCmd = true; 		// set the waiting flag
	pWeta->states.commState  = COMM_STARTED;

	while (pWeta->states.commState != COMM_FINISHED)
	{
			// The Cricket host expects all characters to be echoed, because
			// this was done by the infrared device used for communication
			// with the Cricket robot.
		uint8_t temp;
		if (!hw_serial_available(pWeta->sport))
		{
			hw_time_waitms(10);
			//weta_debug(pWeta, "No data available. Waiting...\r\n");
			continue;
		}
		temp = hw_serial_read_byte(pWeta->sport);
	
		char msg[24];
		//sprintf(msg, "->%02x\r\n", temp);
		//weta_debug(pWeta, msg);						
		hw_serial_write_byte(pWeta->sport, temp); // Programmer expects echo
		
		if(pWeta->states.waitingCmd)
		{
			cmd = temp;
				// test received command and set number of bytes to read
			switch(cmd)
			{
			case cmdCricketCheck:
				remainingChars = 1;	// remaining character received should be 0 (not '0')
				pWeta->states.waitingCmd = false;
				break;

			case cmdSetPointer:
			case cmdReadBytes:
			case cmdWriteBytes:
				remainingChars = 2;
				pWeta->states.waitingCmd = false;
			break;

			case cmdRun:
				weta_debug(pWeta, "Run command received\r\n");
				pWeta->states.runRequest = RUNNING;
				pWeta->states.commState  = COMM_FINISHED;
			break;

			default:
				pWeta->states.commState = COMM_FINISHED;	// should never get here.
				break;
			}
		}
		else
		{
				// receive bytes corresponding to each command
			pWeta->states.waitingCmd = false;
			switch(cmd)
			{
			case cmdCricketCheck:
				if (remainingChars == 1)
				{
					hw_serial_write_byte(pWeta->sport, cmdCricketCheckACK);
					//sprintf(msg, "->%02x\r\n", cmdCricketCheckACK);
					//weta_debug(pWeta, msg);
				}

				pWeta->states.commState = COMM_FINISHED;
				break;


			case cmdSetPointer:
				if (remainingChars == 2)
				{
					remainingChars--;
					start_address = (WetaCodePtr)(temp << 8);
				}
				else
				{
					remainingChars--;
					start_address |= (WetaCodePtr)(temp & 0xFF);
					pWeta->startAddress = start_address;
					pWeta->states.commState = COMM_FINISHED;
					
					sprintf(msg, "Start address is %d\r\n", start_address);
					weta_debug(pWeta, msg);
				}
				break;

			case cmdWriteBytes:
				if (remainingChars == 2)
				{
					remainingChars--;
					byte_count = (WetaCodePtr)temp << 8;
				}
				else if (remainingChars == 1)
				{
					remainingChars--;
					byte_count |= (WetaCodePtr)(temp & 0xFF);
				}
				else
				{
					if (!byte_count_written)
					{
						sprintf(msg, "Writing byte count(%d)\r\n", byte_count);
						weta_debug(pWeta, msg);
							// This writes the byte count at the beginning of the data
						pWeta->store = weta_store_open_write(
							pWeta->hal->code + start_address, 
							byte_count
						);
						if (pWeta->store)
						{
							pWeta->regs.pc = 0;
							byte_count_written = true;
						}
						else
						{
							pWeta->states.commState = COMM_FINISHED;
							break;
						}
							// Reuse the start_address variable as a counter
						start_address = 0;
					}
					hw_serial_write_byte(pWeta->sport, ~temp);
					sprintf(msg, "Writing [%d] = %02x\r\n", start_address, temp);
					weta_debug(pWeta, msg);
					pWeta->regs.pc = weta_store_write_byte(pWeta->store, pWeta->regs.pc, temp);
					start_address++;
					byte_count--;
					if (byte_count == 0)
					{
						pWeta->regs.pc = 0;
						weta_store_flush(pWeta->store);
						weta_store_close(pWeta->store);
						pWeta->store = 0;
						pWeta->states.commState = COMM_FINISHED;
					}
				}
				break;

			case cmdReadBytes:
				if (remainingChars == 2)
				{
					remainingChars--;
					byte_count = (unsigned int)temp << 8;
				}
				else if (remainingChars == 1)
				{
					remainingChars--;
					byte_count |= temp & 0xFF;
						// Once byte_count is received, start sending characters.
					pWeta->store = weta_store_open_read(
						pWeta->hal->code + start_address 
					);
					if (pWeta->store)
					{
						pWeta->regs.pc = 0;
						while (byte_count > 0)
						{
							pWeta->regs.pc = weta_store_read_byte(
								pWeta->store,
								pWeta->regs.pc, &temp);
							hw_serial_write_byte(pWeta->sport, temp);
							hw_serial_write_byte(pWeta->debugsport, temp);
							byte_count--;
						}
					}
					pWeta->states.commState = COMM_FINISHED;
				}
				break;

			default:
				pWeta->states.commState = COMM_FINISHED;	// should never get here
				break;
			}
		}
	}
}

//*******************************************************************************
void exec_loop(Weta* pWeta)
{
	weta_debug(pWeta, "exec_loop()\r\n");
	char szMsg[64];
	
	pWeta->store = weta_store_open_read(pWeta->hal->code + pWeta->startAddress);
	if (pWeta->store)
	{
		pWeta->regs.pc = 0;
	}
	else
	{
		sprintf(szMsg, "weta_store_open_read() failed\r\n");
		weta_debug(pWeta, szMsg);
		pWeta->states.runRequest = STOPPED;
		return;
	}
	
	while (pWeta->states.runRequest == RUNNING)
	{
		//WetaCodePtr loc = pWeta->regs.pc;
		pWeta->regs.pc = weta_store_read_byte(
			pWeta->store,
			pWeta->regs.pc, 
			&pWeta->regs.opCode
			);
		//sprintf(szMsg, "weta_store_read_byte(%d) = %d\r\n", loc, pWeta->regs.opCode);
		//weta_debug(pWeta, szMsg);
		switch (pWeta->regs.opCode)
		{
/*			
#ifdef INCLUDE_LIB_IO
			case OP_IO:
				_ioLib.interpret();
				break;
#endif
#ifdef INCLUDE_LIB_MATH	
			case OP_MATH:
				_mathLib.interpret();
				break;
#endif
#ifdef INCLUDE_LIB_COMM	
			case OP_COMM:
				_commLib.interpret();
				break;
#endif
*/
			case OP_BYTE:
			case OP_INT8:
			case OP_SPAN:
				{
					//Serial.println("int8 ");
					uint8_t value;
					//WetaCodePtr loc = pWeta->regs.pc; // Debugging
					pWeta->regs.pc = weta_store_read_byte(
						pWeta->store,
						pWeta->regs.pc, 
						&value);
					//sprintf(szMsg, "OP_BYTE @ %d = %d\r\n", loc, value);
					//weta_debug(pWeta, szMsg);
					weta_stack_pushUint8(pWeta->stack, value);
				}
				break;
			

			case OP_SHORT:
			case OP_UINT16:
				{
					//Serial.print("int16(");
					uint16_t value;
					pWeta->regs.pc = weta_store_read_uint16(
						pWeta->store,
						pWeta->regs.pc, 
						&value);
						
					//sprintf(szMsg, "OP_SHORT (%d)\r\n", value);
					//weta_debug(pWeta, szMsg);
					//Serial.print(value);
					//Serial.print(") ");
					weta_stack_pushUint16(pWeta->stack, value);
					//.println(stk);
				}
				break;

			case OP_GLOBAL:
				{
					//Serial.print("global: ");
					WetaStackPtr value;
					pWeta->regs.pc = weta_store_read_stackptr(
						pWeta->store,
						pWeta->regs.pc, 
						&value);
					//Serial.println(value);
					weta_stack_pushStackPtr(pWeta->stack, value); //pWeta->stack.push(_stack.getBottom() - value);
				}
				break;

			case OP_INT32:
			case OP_UINT32:
				{
					//Serial.print("int32 ");
					uint32_t value;
					pWeta->regs.pc = weta_store_read_uint32(
						pWeta->store,
						pWeta->regs.pc, 
						&value);
#ifdef SUPPORT_32BIT
					weta_stack_pushUint32(pWeta->stack, value);
#else
					weta_stack_pushUint16(pWeta->stack, (value >> 16) & 0xFFFF);	// High 16 bits
					weta_stack_pushUint16(pWeta->stack, value & 0xFFFF);	// Low 16 bits
#endif
				}
				break;

#ifdef SUPPORT_FLOAT
			case OP_FLOAT:
				{
					//Serial.print("float ");
					float value;
					pWeta->regs.pc = weta_store_read_float(
						pWeta->store, 
						pWeta->regs.pc, 
						&value);
					weta_stack_pushFloat(pWeta->stack, value);
				}
				break;
#endif
#ifdef SUPPORT_DOUBLE
			case OP_DOUBLE:
				{	
					//Serial.print("double ");
					double value;
					pWeta->regs.pc = weta_store_read_double(
						pWeta->store, 
						pWeta->regs.pc, 
						&value);
					weta_stack_pushDouble(pWeta->stack, value);
				}
				break;
#endif
			case OP_BOOL:
				{
					//Serial.print("bool  ");
					uint8_t value;
					pWeta->regs.pc = weta_store_read_byte(
						pWeta->store,
						pWeta->regs.pc, 
						&value);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)!!value);
				}
				break;

			case OP_CPTR:
				{
					//Serial.print("cptr  ");
					WetaCodePtr value;
					pWeta->regs.pc = weta_store_read_codePtr(
						pWeta->store,
						pWeta->regs.pc, 
						&value);
					weta_stack_pushCodePtr(pWeta->stack, value);
				}
				break;
#ifdef SUPPORT_STRING
			case OP_STRING:
				{
					//Serial.print("string: \"");
						// ***KLUDGE WARNING***
						// Borrowing unused (hopefully) stack space as a buffer!
						// (To avoid having to allocate another buffer.
					uint8_t* psz = (uint8_t*)weta_stack_getTopAddress(pWeta->stack);
					uint8_t nextChar;
					int16_t i = -1;
					do
					{
						i++;
						pWeta->regs.pc = weta_store_read_byte(
							pWeta->store,
							pWeta->regs.pc, 
							&nextChar);
						psz[i] = nextChar;
					}
					while (nextChar);
					//Serial.print((char *)psz);
					//Serial.print("\" ");
					//STACKSTATE state;
					//weta_stack_getStackState(_stack, &state);
					//Serial.print("[("); Serial.print(state.top); Serial.print(","); Serial.print(state.stringTop);Serial.print(") -> (");
					weta_stack_pushString(pWeta->stack, psz);
					//weta_stack_getStackState(_stack, &state);
					//Serial.print(state.top); Serial.print(","); Serial.print(state.stringTop);Serial.println(")]");
				}
				break;
	
			case OP_ASCII:
				{
					uint8_t* psz;
					weta_stack_topString(pWeta->stack, &psz);
					uint8_t ascii = psz[0];
					weta_stack_popString(pWeta->stack);
					weta_stack_pushUint8(pWeta->stack, ascii);
				}
				break;

			case OP_STRLEN:
				{
					uint8_t* psz;
					weta_stack_topString(pWeta->stack, &psz);
					uint8_t len = strlen((char*)psz);
					weta_stack_popString(pWeta->stack);
					weta_stack_pushUint8(pWeta->stack, len);
				}
				break;
#endif
			case OP_BEEP:
				//Serial.println("---beep---");
				weta_debug(pWeta, "OP_BEEP\r\n");
				hw_buzzer_beep();
				break;

			case OP_LEDON:
				//Serial.println("---LED on---");
				hw_gpio_set(&pWeta->hal->gpio, LEDUSER, true);
				break;

			case OP_LEDOFF:
				//Serial.println("---LED off---");
				hw_gpio_set(&pWeta->hal->gpio, LEDUSER, false);
				break;


			case OP_WITHINT8:
			case OP_WITHUINT8:
			case OP_WITHINT16:
			case OP_WITHUINT16:
			case OP_WITHBOOL:
			case OP_WITHPTR:
#ifdef SUPPORT_32BIT
			case OP_WITHINT32:
			case OP_WITHUINT32:
#endif
#ifdef SUPPORT_FLOAT
			case OP_WITHFLOAT:
#endif
#ifdef SUPPORT_DOUBLE
			case OP_WITHDOUBLE:
#endif
#ifdef SUPPORT_STRING			
			case OP_WITHSTRING:
#endif			
				pWeta->regs.withCode = pWeta->regs.opCode;
				break;

			case OP_LOCAL:
				{
					//Serial.print("local: local frame (");
					//Serial.print(pWeta->regs.localFrame);
					//Serial.print(") - ");

					WetaStackPtr varOffset;
					pWeta->regs.pc = weta_store_read_uint16(
						pWeta->store,
						pWeta->regs.pc, 
						(uint16_t*)&varOffset);
					weta_stack_pushStackPtr(
						pWeta->stack, 
						(WetaStackPtr)pWeta->regs.localFrame.top + varOffset);

					//Serial.print(varOffset);
					//Serial.print(" = global ");
					//Serial.println(pWeta->regs.localFrame + varOffset);
				}
				break;

			case OP_PARAM:
				{
					//Serial.print("param: ");
					WetaStackPtr varOffset;
					pWeta->regs.pc = weta_store_read_uint16(
						pWeta->store,
						pWeta->regs.pc, 
						(uint16_t*)&varOffset);
					//sprintf(szMsg, "OP_PARAM (%d)\r\n", varOffset);
					//weta_debug(pWeta, szMsg);
						// We have an index into the parameters, which were put on
						// the stack in reverse order before the function call.
						// Calculate the absolute stack offset using the local
						// stack frame offset.
						// Also, the total size of the arguments was pushed last,
						// so we need to step past that too.
						// TODO: Check against the total argument size.
					weta_stack_pushStackPtr(
						pWeta->stack, 
						getArgsLocation(&pWeta->regs) 
						- sizeof(uint8_t) 	// Size of args
						- varOffset		// Offset to the required param 
					);
				}
				break;

			case OP_BLOCK:
				{
					//Serial.print("block ");
					descendBlock(&pWeta->regs);	// Shift the flag to the next block bit
					//char psz[10]; 
					//utoa(pWeta->regs.blockDepthMask, psz, 2);
					//Serial.println(psz);
					uint16_t blockLength;
						// Push address of next instruction (following the block
						// length data)
					weta_stack_pushCodePtr(
						pWeta->stack, 
						(WetaCodePtr)pWeta->regs.pc + sizeof(uint16_t));
					weta_store_read_uint16(pWeta->store, pWeta->regs.pc, &blockLength);
						// Step past the block (tests there will bring execution back)
					pWeta->regs.pc += blockLength;	

				}
				break;

			case OP_EOB:	
				//Serial.println("--eob--");
				setBlockExecuted(&pWeta->regs);	// Set the bit to indicate that this block has executed
				break;
				
			case OP_DO:
				{
					//Serial.println("---do---");
					// OP_DO is like OP_BLOCK except that execution falls through to
					// the top of the block of code unconditionally rather than jump 
					// to the end where some condition is tested.
					// Need to:
					//  - Push the address that the following OP_BLOCK code would 
					//    push if it had been allowed to execute.
					//  - Step past the:
					//	  - The OP_BLOCK code (uint8_t)
					//    - The block size (uint16_t)
					// After going through the code block it should jump back to 
					// the beginning of the OP_BLOCK and loop as usual.
					descendBlock(&pWeta->regs);	// Shift the flag to the next block bit (normally done by OP_BLOCK)
					WetaCodePtr startOfBlock = 
						(WetaCodePtr)pWeta->regs.pc + sizeof(uint8_t) +sizeof(uint16_t);
					weta_stack_pushCodePtr(pWeta->stack, startOfBlock); 
					pWeta->regs.pc = startOfBlock;
				}
				break;

			case OP_WHILE:
				{
					//Serial.print("while ");
					bool  condition;
					WetaCodePtr blockAddr;
					weta_stack_popUint8(pWeta->stack, (uint8_t*)&condition);
					//Serial.println(condition ? "true" : "false");
					//_stack.pop(blockAddr);
					if (condition) // if true then go to the block start address
					{
						weta_stack_topCodePtr(pWeta->stack, &blockAddr);
						pWeta->regs.pc = blockAddr;
					}
					else
					{
						weta_stack_popCodePtr(pWeta->stack, &blockAddr); // Throw it away
						ascendBlock(&pWeta->regs);	// Finished with this block now
					}
				}	
				break;
				
			case OP_REPEAT:
				{
					//Serial.print("repeat ");
					WetaCodePtr blockAddr;
					uint16_t max;
					weta_stack_popCodePtr(pWeta->stack, &blockAddr);
					weta_stack_popUint16(pWeta->stack, &max);
					uint16_t repcount;
					if (!hasBlockExecuted(&pWeta->regs)) // First time around?
					{
						repcount = 1;
						weta_stack_pushUint16(pWeta->stack, repcount);
							// point to the counter we just pushed
						WetaStackPtr slot = weta_stack_getTop(pWeta->stack);
							// Save outer loop's repcount pointer
						weta_stack_pushStackPtr(pWeta->stack, pWeta->regs.repcountLocation);
							// Set it to ours 
						pWeta->regs.repcountLocation = slot;
					}
					else
					{
						weta_stack_getUint16(pWeta->stack, pWeta->regs.repcountLocation, &repcount); // Get counter value
						repcount++;
					}
					//Serial.println(repcount);
					if (repcount <= max)
					{
						weta_stack_setUint16(pWeta->stack, pWeta->regs.repcountLocation, repcount);
						weta_stack_pushUint16(pWeta->stack, max);
						weta_stack_pushCodePtr(pWeta->stack, blockAddr);
						pWeta->regs.pc = blockAddr; // Back to start of block
					}
					else
					{
							// Restore the outer loop's repcount pointer
						weta_stack_popStackPtr(pWeta->stack, &pWeta->regs.repcountLocation);
						weta_stack_popUint16(pWeta->stack, &repcount);	// Dispose of counter
						ascendBlock(&pWeta->regs);	// Finished with this block now
					}
				}
				break;

			case OP_REPCOUNT:
				{
					uint16_t repcount;
					weta_stack_getUint16(pWeta->stack, pWeta->regs.repcountLocation, &repcount);
					weta_stack_pushUint16(pWeta->stack, repcount);
				}
				break;

			case OP_FOR:
				{
					//Serial.println("for ");
						// The counter variable has already been set to the from
						// value, so the from value isn't on the stack.
					WetaCodePtr  blockAddr;
					int16_t      step, to, from;
					WetaStackPtr counterOff;
					int16_t      counterVal;
					weta_stack_popCodePtr(pWeta->stack, &blockAddr); 		
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&step); 
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&to);
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&from); 
					weta_stack_popStackPtr(pWeta->stack, &counterOff); 
					weta_stack_getUint16(pWeta->stack, counterOff, (uint16_t*)&counterVal);

					//Serial.print(counterVal); 
					//Serial.print(" ");
					//Serial.print(to); 
					//Serial.print(" ");
					//Serial.println(step); 

					bool keepGoing;
						// See if this is the first time around
					if (!hasBlockExecuted(&pWeta->regs))
					{
						counterVal = from;
						keepGoing = (step > 0) ? (counterVal <= to) : (counterVal >= to);
					}
					else
					{
						// If step > 0 then assume from < to otherwise assume from > to
						keepGoing = (step > 0) ? (counterVal < to) : (counterVal > to); 
						counterVal += step;
					}
					if (keepGoing)
					{
						weta_stack_setUint16(pWeta->stack, counterOff, (uint16_t)counterVal);
						pWeta->regs.pc = blockAddr; // reiterate
						weta_stack_pushStackPtr(pWeta->stack, counterOff);	// Var offset
						weta_stack_pushUint16(pWeta->stack, (uint16_t)from);	// to
						weta_stack_pushUint16(pWeta->stack, (uint16_t)to);	// to
						weta_stack_pushUint16(pWeta->stack, (uint16_t)step);	// step
						weta_stack_pushCodePtr(pWeta->stack, blockAddr);
					}
					else
					{
						ascendBlock(&pWeta->regs);
					}
				}
				break;


			case OP_IF:
				{
					//Serial.print("if ");
						// If it's the first time through then check the
						// condition
					if (!hasBlockExecuted(&pWeta->regs))
					{
						WetaCodePtr blockAddr;
						bool     condition;
						weta_stack_popCodePtr(pWeta->stack, &blockAddr);  // Block initial address
						weta_stack_popUint8(pWeta->stack, (uint8_t*)&condition); // argument to test
						//Serial.println(condition ? "true" : "false");
						if (condition)
						{
							pWeta->regs.pc = blockAddr;	// Go to start of block
						}
						else
						{
							ascendBlock(&pWeta->regs);
						}
					}
					else

					{
						ascendBlock(&pWeta->regs);
					}
				}
				break;

				// IFELSE starts with address of THEN and ELSE lists (and 
				// CONDITIONAL) on the stack. CONDITIONAL is tested and 
				// appropriate list is run. 
			case OP_IFELSE:
				{
					//Serial.print("ifelse ");
					WetaCodePtr elseBlock;
					weta_stack_popCodePtr(pWeta->stack, &elseBlock);  // ELSE block start
						// Note that descendBlock() will have been called twice
						// the first time around (once for each block as it was skipped).
					ascendBlock(&pWeta->regs); // Remove the else block flag...
						// ...and use the then block flag for both purposes
					if (!hasBlockExecuted(&pWeta->regs))
					{
						WetaCodePtr thenBlock;
						bool     condition;
						weta_stack_popCodePtr(pWeta->stack, &thenBlock); // THEN block start
						weta_stack_popUint8(pWeta->stack, (uint8_t*)&condition); 	  // argument to test
						if (condition)
						{
							//Serial.println("(then)");
							pWeta->regs.pc = thenBlock; // Go to then block
								// The ELSE address will get pushed again when
								// execution falls into the ELSE block after
								// exiting the THEN block.
								// Another else block will be descended into  
								// as well, and ascended away again above.
								// The eob code will be encountered at the end
								// of the then block, and that will set the
								// executed flag.
						}
						else
						{
							//Serial.println("(else)");
							pWeta->regs.pc = elseBlock;	  // Go to else block
							weta_stack_pushCodePtr(pWeta->stack, (WetaCodePtr)0); // Push fake ELSE to balance
								// Borrow the then block flag and set it now as
								// executed since it won't actually be set
								// otherwise.
							setBlockExecuted(&pWeta->regs);
								// Descend the else block now, as 
								// this also won't be done in the block's code.
							descendBlock(&pWeta->regs);
						}
					}
					else
					{
						//weta_stack_popCodePtr(_stack, &elseBlock);  // dispose of unrequired address
						ascendBlock(&pWeta->regs); // ie. the then block
					}
				}
				break;

			case OP_PUSH:
				{
					//Serial.print("push ");
					uint8_t amount;
					weta_stack_popUint8(pWeta->stack, &amount);
					weta_stack_pushn(pWeta->stack, (WetaStackPtr)amount);
				}
				break;
			
			case OP_POP:
				{
					//Serial.print("pop ");
					uint8_t amount;
					weta_stack_popUint8(pWeta->stack, &amount);
					weta_stack_popn(pWeta->stack, (WetaStackPtr)amount);
				}
				break;
			
			case OP_CHKPOINT:
					// Save the stack context (top etc.) This is necessary
					// because checkpoints can be recursive.
				weta_stack_pushStackState(pWeta->stack, &pWeta->regs.checkPoint);
					//i.e. OF the stack, not from the stack
				weta_stack_getStackState(pWeta->stack, &pWeta->regs.checkPoint);
				break;

			case OP_ROLLBACK:
					// Set stack to where it was at the checkpoint (throw away
					// anything that was put onto the stack since then).
				weta_stack_setStackState(pWeta->stack, &pWeta->regs.checkPoint);
					// Now the state that was saved can be restored
				weta_stack_popStackState(pWeta->stack, &pWeta->regs.checkPoint);
				break;
				
			case OP_CALL:
				{
					//Serial.println("call");
					WetaCodePtr procAddr;
					weta_stack_popCodePtr(pWeta->stack, &procAddr);	// Get the function location
						// Save the current code location for the return
					weta_stack_pushCodePtr(pWeta->stack, (WetaCodePtr)pWeta->regs.pc);	
					pWeta->regs.pc = procAddr;  // Now jump to the function
				}
				break;

			case OP_BEGIN:
				//Serial.println("begin");
				pushRegisters(pWeta);	// Save state of the caller
				
					// Block (e.g. if, while, for etc.) states (ie. executed)
					// have been saved byt pushRegisters() above. Reset for this procedure. 
				pWeta->regs.blockDepthMask   = 0;
				pWeta->regs.blocksExecuted   = 0;
				//weta_stack_getStackState(_stack, &pWeta->regs.localFrame); // = weta_stack_getTop(_stack);
				
				//Serial.println(pWeta->regs.localFrame);
				break;

			case OP_ENTER:
					// Save the caller's local frame then get the frame for the
					// current procedure. MUST be called immediately after 
					// OP_BEGIN if there are EITHER call arguments or local 
					// variables.
				weta_stack_pushStackState(pWeta->stack, &pWeta->regs.localFrame);
					// i.e. OF the stack, not from the stack
				weta_stack_getStackState(pWeta->stack, &pWeta->regs.localFrame);
				break;

			case OP_LEAVE:
					// Unwind the stack to the beginning of the local frame.
					// Call immediately before OP_RETURN if OP_ENTER was called
					// for the current procedure.
				weta_stack_setStackState(pWeta->stack, &pWeta->regs.localFrame);
				weta_stack_popStackState(pWeta->stack, &pWeta->regs.localFrame);
				break;

			case OP_RETURN:
				{
					//Serial.print("return ");

						//Unwind the stack to the beginning of the local frame
					//weta_stack_setStackState(_stack, &pWeta->regs.localFrame);
					popRegisters(pWeta);
					
					WetaCodePtr returnAddr;
					weta_stack_popCodePtr(pWeta->stack, &returnAddr);	// Get the return address
					//_stack.pop(pWeta->regs.argsLoc);	// Restore the param location for the calling function
					pWeta->regs.pc = returnAddr;
				}
				break;
			
			case OP_EXIT:
				weta_reset(pWeta);
				//Serial.println("---exit---");
				break;

			case OP_LOOP:
				{
					//Serial.println("---loop---");
					WetaCodePtr blockAddr;
					weta_stack_topCodePtr(pWeta->stack, &blockAddr); 
						// Go back to the top of the block unconditionally
					pWeta->regs.pc = blockAddr;

				}
				break;


			case OP_WAIT:
				{
					//Serial.println("---wait---");
					uint16_t millis;
					weta_stack_popUint16(pWeta->stack, &millis);
					hw_time_waitms(millis);
				}
				break;

			case OP_TIMER:
				//Serial.print("timer ");
					// ChibiOS call (TO DO: hide this)
				weta_stack_pushUint16(pWeta->stack, (uint16_t)(hw_time_now()- pWeta->timerStart)); 
				break;

			case OP_RESETT:
				//Serial.println("---resett---");
				pWeta->timerStart = hw_time_now(); // ChibiOS call (TO DO: hide this)
				break;

			case OP_RANDOM:
				//Serial.print("random ");
				weta_stack_pushUint16(pWeta->stack, (uint16_t)((32767 * rand()) / RAND_MAX));
				break;

			case OP_RANDOMXY:
				{
					//Serial.print("randomxy ");
					int16_t x;
					int16_t y;
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&y);
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&x);
					 	// Nasty hack. 
						// Assumes y > x.
						// Use the difference as the range, then add x to shift
						// the result back into the desired range.
						// Note that though the result is cast as unsigned,
						// when it is popped it can still be interpreted as signed.
					weta_stack_pushUint16(
						pWeta->stack, 
						(uint16_t)((((y-x) * rand()) / RAND_MAX) + x)
					);
				}
				break;

			case OP_MOTORS:
				{
					//Serial.print("motors ");
					uint8_t selected;
					weta_stack_popUint8(pWeta->stack, &selected);
					hw_motor_select(&pWeta->hal->motors, selected);
				}
				break;
				
			case OP_THISWAY:
				//Serial.print("---thisway---");
				hw_motor_direction(&pWeta->hal->motors, MOTOR_THIS_WAY);
				break;


			case OP_THATWAY:
				//Serial.print("---thatway---");
				hw_motor_direction(&pWeta->hal->motors, MOTOR_THAT_WAY);
				break;

			case OP_RD:
				//Serial.print("---rd---");
				hw_motor_reverse(&pWeta->hal->motors);
				break;
				
			case OP_SETPOWER:
				{
					uint8_t power;
					weta_stack_popUint8(pWeta->stack, &power);
					//sprintf(szMsg, "OP_SETPOWER to %d\r\n", power);
					//weta_debug(pWeta, szMsg);
					hw_motor_power(&pWeta->hal->motors, power);
				}
				break;


			case OP_BRAKE:
				//Serial.println("---brake---");
				hw_motor_brake(&pWeta->hal->motors, true);
				break;

			case OP_ON:
				//weta_debug(pWeta, "OP_ON\r\n");
				hw_motor_on(&pWeta->hal->motors, true);
				break;

			case OP_ONFOR:
				{
					//Serial.print("onfor(");
					uint16_t tenths;
					weta_stack_popUint16(pWeta->stack, &tenths);
					hw_motor_on(&pWeta->hal->motors, true);
					hw_time_waitms(tenths * 100);
					hw_motor_on(&pWeta->hal->motors, false);
				}
				break;

			case OP_OFF:
				//weta_debug(pWeta, "OP_OFF\r\n");
				hw_motor_on(&pWeta->hal->motors, false);
				break;
				
			case OP_SERVOS:
				{
					uint8_t selected;
					weta_stack_popUint8(pWeta->stack, &selected);
					sprintf(szMsg, "OP_SERVOS (%d)\r\n", selected);
					weta_debug(pWeta, szMsg);
					hw_servo_select(&pWeta->hal->servos, selected);
				}
				break;
				
			case OP_SETSVH:
				{
					int16_t heading;
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&heading);
					sprintf(szMsg, "OP_SETSVH (%d)\r\n", heading);
					weta_debug(pWeta, szMsg);
					hw_servo_set_position(&pWeta->hal->servos, heading);
				}
				break;
				
			case OP_SVR:
			case OP_SVL:
				break;	// TO DO!
			

			case OP_SENSOR1:
			case OP_SENSOR2:
				{
					int16_t value = 0;
					hw_adc_get(pWeta->regs.opCode - OP_SENSOR1, &value);
					//sprintf(szMsg, "OP_SENSOR(%d) = %d\r\n", pWeta->regs.opCode - OP_SENSOR1, value);
					//weta_debug(pWeta, szMsg);
					weta_stack_pushUint16(
						pWeta->stack, 
						(uint16_t)value
					);
				}
				break;
				
			case OP_SENSOR3:
			case OP_SENSOR4:
			case OP_SENSOR5:
			case OP_SENSOR6:
			case OP_SENSOR7:
			case OP_SENSOR8:
				{
					int16_t value = 0;
					hw_adc_get(pWeta->regs.opCode - OP_SENSOR3 + 2, &value);
					//sprintf(szMsg, "OP_SENSOR(%d) = %d\r\n", pWeta->regs.opCode - OP_SENSOR3 + 2, value);
					//weta_stack_pushUint16(pWeta->stack, (uint16_t)value);
				}
				break;

			case OP_AIN:
				{
					//Serial.print("ain ");
					uint8_t input;
					weta_stack_popUint8(pWeta->stack, &input);
					int16_t value = 0;
					hw_adc_get(input, &value);
					weta_stack_pushUint16(pWeta->stack,(uint16_t)value);
				}
				break;

			case OP_AOUT:
				{
					//Serial.print("aout ");
					uint8_t output;
					uint8_t value;
					weta_stack_popUint8(pWeta->stack, &output);
					weta_stack_popUint8(pWeta->stack, &value);
					hw_dac_set(output, (uint16_t)value);
				}
				break;

			case OP_SWITCH1:
			case OP_SWITCH2:
				{
					bool value = false;
					hw_gpio_get(
						&pWeta->hal->gpio, 
						pWeta->regs.opCode - OP_SWITCH1 + SWITCH1, 
						&value
					);
					//sprintf(szMsg, "OP_SWITCH(%d) = %d\r\n", pWeta->regs.opCode - OP_SWITCH1 + SWITCH1, value);
					//weta_debug(pWeta, szMsg);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)value);
				}
				break;
				
			case OP_SWITCH3:
			case OP_SWITCH4:
			case OP_SWITCH5:
			case OP_SWITCH6:
			case OP_SWITCH7:
			case OP_SWITCH8:
				{
					bool value = false;
					hw_gpio_get(
						&pWeta->hal->gpio, 
						pWeta->regs.opCode - OP_SWITCH3 + SWITCH3, 
						&value
					);
					//sprintf(szMsg, "OP_SWITCH(%d) = %d\r\n", pWeta->regs.opCode - OP_SWITCH3 + SWITCH3, value);
					//weta_debug(pWeta, szMsg);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)value);
				}
				break;
			
			case OP_NOT:
				{
					uint8_t rhs;
					weta_stack_popUint8(pWeta->stack, &rhs);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)!rhs);
				}
				break;
			
			case OP_AND:
				{
					uint8_t rhs, lhs;
					weta_stack_popUint8(pWeta->stack, &rhs);
					weta_stack_popUint8(pWeta->stack, &lhs);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)(!!lhs && !!rhs));
				}
				break;

			case OP_OR:
				{
					uint8_t rhs, lhs;
					weta_stack_popUint8(pWeta->stack, &rhs);
					weta_stack_popUint8(pWeta->stack, &lhs);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)(!!lhs || !!rhs));
				}
				break;

			case OP_XOR:
				{
					uint8_t rhs, lhs;
					weta_stack_popUint8(pWeta->stack, &rhs);
					weta_stack_popUint8(pWeta->stack, &lhs);
						// No logical XOR operator in C (only bitwise)
					weta_stack_pushUint8(
						pWeta->stack, 
						(uint8_t)((!!lhs && !rhs) || (!lhs && !!rhs))
					);
				}
				break;

			case OP_DIN:
				{
					//Serial.print("din ");
					uint8_t input;
					weta_stack_popUint8(pWeta->stack, &input);
					bool value = false;
					hw_gpio_get(&pWeta->hal->gpio, input, &value);
					weta_stack_pushUint8(pWeta->stack,(uint8_t)value);
				}
				break;

			case OP_DOUT:
				{
					//Serial.print("dout ");
					uint8_t output;
					uint8_t value;
					weta_stack_popUint8(pWeta->stack, &output);
					weta_stack_popUint8(pWeta->stack, &value);
					hw_gpio_set(&pWeta->hal->gpio, output, value);
				}
				break;
			
			case OP_BTOS:
				{
					int8_t value;
					weta_stack_popUint8(pWeta->stack, (uint8_t*)&value);
					weta_stack_pushUint16(pWeta->stack,(uint16_t)value);
				}
				break;
			case OP_UBTOS:
				{
					uint8_t value;
					weta_stack_popUint8(pWeta->stack, &value);
					weta_stack_pushUint16(pWeta->stack,(uint16_t)value);
				}
				break;
			case OP_STOB:	// and OP_USTOB
				{
					//Serial.print("stob: ");
					uint16_t value;
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&value);
					//Serial.println(value);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)value);
					
				}
				break;
#ifdef SUPPORT_32BIT
			case OP_BTOI:
				{
					int8_t value;
					weta_stack_popUint8(pWeta->stack, (uint8_t*)&value);
					weta_stack_pushUint32(pWeta->stack,(uint32_t)value);
				}
				break;
			case OP_UBTOI:
				{
					uint8_t value;
					weta_stack_popUint8(pWeta->stack, &value);
					weta_stack_pushUint32(pWeta->stack,(uint32_t)value);
				}
				break;
			case OP_STOI:
				{
					int16_t value;
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&value);
					weta_stack_pushUint32(pWeta->stack,(uint32_t)value);
				}
				break;
			case OP_USTOI:
				{
					uint16_t value;
					weta_stack_popUint16(pWeta->stack, &value);
					weta_stack_pushUint32(pWeta->stack,(uint32_t)value);
				}
				break;
			case OP_ITOB: // and OP_UITOB
				{
					int32_t value;
					weta_stack_popUint32(pWeta->stack, (uint32_t*)&value);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)value);
				}
				break;
			case OP_ITOS: //and OP_UITOS

				{
					int32_t value;
					weta_stack_popUint32(pWeta->stack, (uint32_t*)&value);
					weta_stack_pushUint16(pWeta->stack, (uint16_t)value);
				}
				break;
#endif
#ifdef SUPPORT_FLOAT
			case OP_BTOF:
				{
					int8_t value;
					weta_stack_popUint8(pWeta->stack, (uint8_t*)&value);
					weta_stack_pushFloat(pWeta->stack, (float)value);
				}
				break;
			case OP_UBTOF:
				{
					uint8_t value;
					weta_stack_popUint8(pWeta->stack, &value);
					weta_stack_pushFloat(pWeta->stack, (float)value);
				}
				break;
			case OP_STOF:
				{
					int16_t value;
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&value);
					weta_stack_pushFloat(pWeta->stack, (float)value);
				}
				break;
			case OP_USTOF:
				{
					uint16_t value;
					weta_stack_popUint16(pWeta->stack, &value);
					weta_stack_pushFloat(pWeta->stack, (float)value);
				}
				break;
			case OP_FTOB:
				{
					float value;
					weta_stack_popFloat(pWeta->stack, &value);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)value);
				}
				break;
			case OP_FTOS:
				{
					float value;
					weta_stack_popFloat(pWeta->stack, &value);
					weta_stack_pushUint16(pWeta->stack, (uint16_t)value);
				}
				break;
#endif
#ifdef SUPPORT_DOUBLE
			case OP_BTOD:
				{
					int8_t value;
					weta_stack_popUint8(pWeta->stack, (uint8_t*)&value);
					weta_stack_pushDouble(pWeta->stack, (double)value);
				}
				break;
			case OP_UBTOD:
				{
					uint8_t value;
					weta_stack_popUint8(pWeta->stack, &value);
					weta_stack_pushDouble(pWeta->stack, (double)value);
				}
				break;
			case OP_STOD:
				{
					int16_t value;
					weta_stack_popUint16(pWeta->stack, (uint16_t*)&value);
					weta_stack_pushDouble(pWeta->stack, (double)value);
				}
				break;
			case OP_USTOD:
				{
					uint16_t value;
					weta_stack_popUint8(pWeta->stack, (uint8_t*)&value);
					weta_stack_pushDouble(pWeta->stack, (double)value);
				}
				break;
			case OP_DTOB:
				{
					double value;
					weta_stack_popDouble(pWeta->stack, &value);
					weta_stack_pushUint8(pWeta->stack, (uint8_t)value);
				}
				break;
			case OP_DTOS:
				{
					double value;
					weta_stack_popDouble(pWeta->stack, &value);
					weta_stack_pushUint16(pWeta->stack, (uint16_t)value);
				}
				break;
#endif
#if defined(SUPPORT_DOUBLE) && defined(SUPPORT_32BIT)
			case OP_ITOD:
				{
					int32_t value;
					weta_stack_popUint32(pWeta->stack, (uint32_t*)&value);
					weta_stack_pushDouble(pWeta->stack, (double)value);
				}
				break;
			case OP_UITOD:
				{
					uint32_t value;
					weta_stack_popUint32(pWeta->stack, &value);
					weta_stack_pushDouble(pWeta->stack, (double)value);
				}
				break;
			case OP_DTOI:
				{
					double value;
					weta_stack_popDouble(pWeta->stack, &value);
					weta_stack_pushUint32(pWeta->stack, (uint32_t)value);
				}
				break;
#endif
#if defined(SUPPORT_DOUBLE) && defined(SUPPORT_FLOAT)
			case OP_FTOD:
				{
					float value;
					weta_stack_popFloat(pWeta->stack, &value);
					weta_stack_pushDouble(pWeta->stack, (double)value);
				}
				break;
			case OP_DTOF:
				{
					double value;
					weta_stack_popDouble(pWeta->stack, &value);
					weta_stack_pushFloat(pWeta->stack, (float)value);
				}
				break;
#endif
#if defined(SUPPORT_FLOAT) && defined(SUPPORT_32BIT)			
			case OP_ITOF:
				{
					int32_t value;
					weta_stack_popUint32(pWeta->stack, (uint32_t*)&value);
					weta_stack_pushFloat(pWeta->stack, (float)value);
				}
				break;
			case OP_UITOF:
				{
					uint32_t value;
					weta_stack_popUint32(pWeta->stack, &value);
					weta_stack_pushFloat(pWeta->stack, (float)value);
				}
				break;
			case OP_FTOI:
				{
					float value;
					weta_stack_popFloat(pWeta->stack, &value);
					weta_stack_pushUint32(pWeta->stack, (uint32_t)value);
				}
				break;
#endif			

			case OP_I2CSTART:
				//weta_debug(pWeta, "OP_I2CSTART\r\n");
				hw_i2c_start();
				break;

			case OP_I2CSTOP:
				//weta_debug(pWeta, "OP_I2CSTOP\r\n");
				hw_i2c_stop();
				break;

			case OP_I2CWRITE:
				{
					//weta_debug(pWeta, "OP_I2CWRITE\r\n");
					uint8_t  slaveAddr;
					uint32_t registerAddr = 0;
					uint8_t  registerAddrWidth;
					uint16_t txBuffLocation;
					uint8_t  txNumBytes;
					weta_stack_popUint8(pWeta->stack, &slaveAddr);
					weta_stack_popUint8(pWeta->stack, &registerAddrWidth);
					if (registerAddrWidth == 1)
						weta_stack_popUint8(pWeta->stack, (uint8_t*)&registerAddr);
					else if (registerAddrWidth == 2)
						weta_stack_popUint16(pWeta->stack, (uint16_t*)&registerAddr);
#ifdef SUPPORT_32BIT
					else if (registerAddrWidth == 3 || registerAddrWidth == 4)
						weta_stack_popUint32(pWeta->stack, &registerAddr);
#endif						
					
					weta_stack_popUint16(pWeta->stack, &txBuffLocation);
					weta_stack_popUint8(pWeta->stack, &txNumBytes);
					
					uint8_t* buffAddr = (uint8_t*)weta_stack_getStackAddress(pWeta->stack, txBuffLocation);
					
					uint8_t rc = hw_i2c_write(
						slaveAddr, 
						registerAddr, 
						registerAddrWidth, 
						buffAddr,   //(uint8_t*)weta_stack_getStackAddress(pWeta->stack, txBuffLocation), 
						(uint32_t)txNumBytes
					);
					sprintf(szMsg, "hw_i2c_write(%d, %d, %d, %d, %d) = %d\r\n", 
							slaveAddr, 
							registerAddr,
							registerAddrWidth,
							(uint8_t)*buffAddr,
							txNumBytes,
							rc);
					weta_debug(pWeta, szMsg);
				}
				break;

			case OP_I2CREAD:
				{
					weta_debug(pWeta, "OP_I2CREAD\r\n");
					uint8_t  slaveAddr;
					uint32_t registerAddr = 0;
					uint8_t  registerAddrWidth;
					uint16_t rxBuffLocation;
					uint8_t  rxNumBytes;
					weta_stack_popUint8(pWeta->stack, &slaveAddr);
					weta_stack_popUint8(pWeta->stack, &registerAddrWidth);
						// A register address width of 0 means that no register
						// address is popped off the stack
					if (registerAddrWidth == 1)
						weta_stack_popUint8(pWeta->stack, (uint8_t*)&registerAddr);
					else if (registerAddrWidth == 2)
						weta_stack_popUint16(pWeta->stack, (uint16_t*)&registerAddr);
#ifdef SUPPORT_32BIT
					else if (registerAddrWidth == 4)
						weta_stack_popUint32(pWeta->stack, &registerAddr);
#endif
					weta_stack_popUint16(pWeta->stack, &rxBuffLocation);
					weta_stack_popUint8(pWeta->stack, &rxNumBytes);
					
					uint8_t* buffAddr = (uint8_t*)weta_stack_getStackAddress(pWeta->stack, rxBuffLocation);
					//uint8_t rc = 42;
					
					uint8_t rc = hw_i2c_read(
						slaveAddr, 
						registerAddr, 
						registerAddrWidth, 
						buffAddr,  //(uint8_t*)weta_stack_getStackAddress(pWeta->stack, rxBuffLocation), 
						(uint32_t)rxNumBytes
					);
					
					sprintf(szMsg, "hw_i2c_read(%d, %d, %d, %d, %d) = %d\r\n", 
							slaveAddr, 
							registerAddr,
							registerAddrWidth,
							(uint8_t)*buffAddr,
							rxNumBytes,
							rc);
					weta_debug(pWeta, szMsg);
				}
				break;
#ifdef SUPPORT_32BIT
			case OP_I2CERR:
				{
					//Serial.println("---i2cerr---");
						// Atmel TWID API doesn't make error codes available.
						// Just return no error.
					weta_stack_pushUint32(pWeta->stack, 0);
				}
				break;
#endif
			case OP_WAITUNTIL:
			case OP_RECORD:
			case OP_RECALL:
			case OP_RESETDP:
			case OP_SETDP:
			case OP_ERASE:
			
					// TODO!!!
				break;
				
			default:
					// All of the type specific codes are dealt with here
				if (!with_current_type(pWeta))
				{
					//beep();	// Just an indication for now.
					//Serial.print("unrecognised opcode: ");
					//Serial.println(pWeta->regs.opCode);
				}
				break;

		}
	}
	weta_store_close(pWeta->store);
}

//*******************************************************************************
void 
init_registers(Registers* regs)
{
	regs->pc               = 0;
	regs->opCode           = OP_INVALID;
	regs->withCode         = OP_WITHINT16; // Gogo board default
	regs->repcountLocation = ~0;
	regs->blockDepthMask   = 0;
	regs->blocksExecuted   = 0;
}

//*******************************************************************************
void 
init_states(WetaStates* states)
{
	states->runRequest   = STOPPED;
	states->machineState = READY;
	states->commState    = COMM_IDLE;
	states->waitingCmd   = false;
}

WetaStackPtr 
getArgsLocation(Registers* pRegs)
{
	return pRegs->localFrame.top 
			- sizeof(uint8_t) * 2	//Block states
			//- sizeof(STACKSTATE)	// Saved checkpoint from outer context
			- sizeof(STACKSTATE)	// Saved localFrame from outer context
			- sizeof(WetaCodePtr);	// Return address
}

void 
descendBlock(Registers* pRegs)
{
	if (pRegs->blockDepthMask == 0)
		pRegs->blockDepthMask = 1;
	else
		pRegs->blockDepthMask <<= 1;
}

void 
ascendBlock(Registers* pRegs)
{
	pRegs->blocksExecuted &= ~pRegs->blockDepthMask; // Clear the executed flag
	pRegs->blockDepthMask >>= 1; // Back to the next block outwards
}

void 
setBlockExecuted(Registers* pRegs)
{
	pRegs->blocksExecuted |= pRegs->blockDepthMask;
}

bool 
hasBlockExecuted(Registers* pRegs)
{
	return (pRegs->blocksExecuted & pRegs->blockDepthMask) == pRegs->blockDepthMask;
}

void    
pushRegisters(Weta* pWeta)
{
	weta_stack_pushUint8(pWeta->stack, pWeta->regs.blockDepthMask);
	weta_stack_pushUint8(pWeta->stack, pWeta->regs.blocksExecuted);
}

void    
popRegisters(Weta* pWeta)
{
	weta_stack_popUint8(pWeta->stack, &pWeta->regs.blocksExecuted);
	weta_stack_popUint8(pWeta->stack, &pWeta->regs.blockDepthMask);
}

bool 
with_current_type(Weta* pWeta)
{
	switch (pWeta->regs.withCode)
	{
	case OP_WITHINT8:	return with_int8(pWeta);
	case OP_WITHUINT8:	return with_uint8(pWeta);
	case OP_WITHINT16:	return with_int16(pWeta);
	case OP_WITHUINT16:	return with_uint16(pWeta);
	case OP_WITHBOOL:	return with_bool(pWeta);
	case OP_WITHPTR:	return with_stackptr(pWeta);
#ifdef SUPPORT_32BIT
	case OP_WITHINT32:	return with_int32(pWeta);
	case OP_WITHUINT32:	return with_uint32(pWeta);
#endif
#ifdef SUPPORT_FLOAT
	case OP_WITHFLOAT:	return with_float(pWeta);
#endif
#ifdef SUPPORT_DOUBLE
	case OP_WITHDOUBLE:	return with_double(pWeta);
#endif
#ifdef SUPPORT_STRING
	case OP_WITHSTRING:	return with_string(pWeta);
#endif
	
	}
	return false;
}
#include "./hw/hw_endian.h"
#include "./type_handlers/with_uint8.c"
#include "./type_handlers/with_int8.c"
#include "./type_handlers/with_uint16.c"
#include "./type_handlers/with_int16.c"
#include "./type_handlers/with_bool.c"
#include "./type_handlers/with_stackptr.c"
#ifdef SUPPORT_32BIT
#include "./type_handlers/with_uint32.c"
#include "./type_handlers/with_int32.c"
#endif
#ifdef SUPPORT_FLOAT
#include "./type_handlers/with_float.c"
#endif 
#ifdef SUPPORT_DOUBLE
#include "./type_handlers/with_double.c"
#endif
#ifdef SUPPORT_STRING
#include "./type_handlers/with_string.c"
#endif
