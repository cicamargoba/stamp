// file: pwm.c
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "imx233.h"


int main(int argc, char **argv) {

   // If the name of the offset ends in CLR, the bits that are hi will be set to 0.
   // If the name of the offset ends in SET, the bits that are hi will be set to 1.
   // If the name of the offset ends in TOG, the bits that are hi will be toggled.
   
   // Change the function of the processor pins
   imx233_wr(HW_PINCTRL_MUXSEL2_SET, 0x0000000F);// SELECCIONADO PINES LCD0 Y LCD1 * aprobado
   
   imx233_wr(HW_PINCTRL_MUXSEL0_SET, 0x00000003);
   imx233_wr(HW_PINCTRL_MUXSEL1_SET, 0x000000C0); // ENCENDER EL BANCO 0 AL PARECER PARA HABILITAR TRANSISTOR

   
   
   // Set OUTPUT
   imx233_wr(HW_PINCTRL_DOE1_SET, 0x00000001);	// Configura como salida el pin 0 del banco 1
   imx233_wr(HW_PINCTRL_DOE1_CLR, 0x00000002);	// Configura como entrada el pin 1 del banco 1
   imx233_wr(HW_PINCTRL_PULL1_SET, 0x00000002); // habilitar resistencias de pull up *
   

   imx233_wr(HW_PINCTRL_DOE0_SET, 0x00080001);	// LOS CONFIGURA COMO SALIDA LOS PINES 0 Y 20 banco 0
   
   // write the  LED and NMOS pin
   imx233_wr(HW_PINCTRL_DOUT0_SET, 0x00080000); // ESTABLECER, UN VALOR CONSTANTE PARA EL PIN, (LA CORRIENTE DEL TRANSISTOR)


while(1){

	//usleep(1000*1000*1);
	imx233_wr(HW_PINCTRL_DOUT1_SET, 0x00000001); // Establece en alto el pin de salida, pin 0 banco 1
	double lectura;
	lectura = imx233_rd(HW_PINCTRL_DIN1)&0x02;
	  printf("%d \n",lectura);      

	if(lectura==2)
	{
	 imx233_wr(HW_PINCTRL_DOUT0_SET, 0x00000001);
	 printf("on\n");
	 usleep(1000*1000*1);
	
	}
	else
	{
	 imx233_wr(HW_PINCTRL_DOUT0_CLR, 0x00000001); // Establece en bajo el pin de salida, pin 0 banco 1
	 printf("off\n");
	 usleep(1000*1000*1);
	 
	}

}

}

