// Reveal images based on link that specifies
// image ID on th page & a new image's url or array element id.
function swap(imageName,url) {
    if (document.images) {
        (document.images[imageName].src = url);
    }
}

// SOME OF THESE ARE NOW UNUSED -- TAKE OUT TO PREVENT UNNECESSARY PRELOADING.
var largeButtons = new Array();
    largeButtons[0] = new Image(45,45);
    largeButtons[1] = new Image(45,45);
    largeButtons[2] = new Image(45,45);

    largeButtons[0].src = '/images/global/button-embedded-systems.png';
    largeButtons[1].src = '/images/global/button-embedded-systems-hover.png';
    largeButtons[2].src = '/images/global/button-embedded-systems-active.png';

// The second two are for the sidebar "order" button.
var cartButtons = new Array();
    cartButtons[0] = new Image(90,90);  // 0 and 1 are now unused.
    cartButtons[1] = new Image(90,90);
    cartButtons[2] = new Image(45,45);
    cartButtons[3] = new Image(45,45);
    cartButtons[4] = new Image(90,90);
    cartButtons[5] = new Image(45,45);

    cartButtons[0].src = '/images/products/button-cart.png';
    cartButtons[1].src = '/images/products/button-cart-hover.png';
    cartButtons[2].src = '/images/products/button-cart-mini.png';
    cartButtons[3].src = '/images/products/button-cart-mini-hover.png';
    cartButtons[4].src = '/images/products/button-cart-active.png';
    cartButtons[5].src = '/images/products/button-cart-mini-active.png';

/* JFAR ABSTRACTS, called in /jfar.html */
var currAbstract = "";
function toggleBlk(id) { // id = the unique ID of a block-level element.
    if (document.getElementById(id).style.display == "block") {
        hideDiv(id);
    } else {
        if ((document.getElementById(id) != currAbstract) && (currAbstract != "")) {
            hideDiv(currAbstract);
        }
        showDiv(id);
        currAbstract = id;
    }
}
/* END: JFAR */

function hideDiv(id) {      // Hide a named div.
  document.getElementById(id).style.display = 'none';
}
function showDiv(id) {      // Hide a named div.
  document.getElementById(id).style.display = 'block';
}
function showInline(id) {      // Hide a named inline element.
  document.getElementById(id).style.display = 'inline';
}
function toggleShowDiv(id) {
  if (document.getElementById(id).style.display == 'none') {
      document.getElementById(id).style.display = 'block';
  } else {
      document.getElementById(id).style.display = 'none';
  }
}
var currDiv = "";
function swapDiv(id) {
  if (currDiv != "") {
      hideDiv(currDiv);
  }
  if (currDiv == id) { // Allow user to toggle-off the display of the current div.
      hideDiv(currDiv);
      currDiv = "";
  } else {
      showDiv(id);
      currDiv = id;
  }
}

var currMenu = "";
var currMenuTimer = "";
var currSubmenu = "";
var currSubmenuTimer = "";

function delayHideMenu(currMenu) { // Leave menu visible long enough to cancel the 'hide' action if
  if (currMenu != "") {
      currMenuTimer = window.setTimeout("hideDiv(currMenu);",250);
  }
}

function delayHideSubmenu(currSubmenu) { // Leave submenu visible long enough to cancel the 'hide' action if...
  if (currSubmenu != "") {
      currSubmenuTimer = window.setTimeout("hideDiv(currSubmenu);",250);
  }
}

// Every submenu has the same Y as its parent link (menu$DivName), and
// an X equal to (the X of its parent link DIV + the width of its parent link DIV).
function alignAndShowMenu(id,offsetLeft,offsetTop,idParent) {    // Recalculates left edge of a menu each time it's displayed.
//  clearTimeout(currSubmenuTimer);
//  currSubmenuTimer = "";
  clearTimeout(currMenuTimer);
  currMenuTimer = "";

  if (offsetTop) {  // Presence of this parameter tells us ID is a submenu's DIV container.
      if ((currSubmenu != id ) && (currSubmenu != "")) {  // Force hideDiv() if new submenu is invoked before another one hides.
           hideDiv(currSubmenu);
      }
      document.getElementById(id).style.top = offsetTop + "px";  // Submenus get their y-position here.
      currSubmenu = id;
  } else {  // It's not a submenu...
     if ((currMenu != id) && (currMenu != "")) {
          hideDiv(currMenu);
     }
     currMenu = id;
  }
  document.getElementById(id).style.left = offsetLeft + "px";   // Menus and submenus get their x-position here.
  showDiv(id);
}

// highlightLink() is used by mini-nav on resources/apps page.
function highlightLink(id) {
  var highlightLinkBGcolor = "#cc9999";
  var normalLinkBGcolor = "#cccccc";

  document.getElementById(currLink).style.background = normalLinkBGcolor;
  currLink = id;
  document.getElementById(id).style.background = highlightLinkBGcolor;
}

function myVoid() {
  return;
}

// Custom clear function; simply reloads original page.
function clearFormFields() {
  document.location = document.location;
}

/* ***THIS FAILSAFE INLINE CSS SHOULD NO LONGER BE NEEDED BY THE BROWSERS WE SUPPORT; IF THINGS APPEAR TO BREAK FOR MIDLE-AGED BROWSERS, TRY RESTORING THIS.
if (!document.getElementById) {
  document.write('<style type="text/css">div#menubar, div#menu1, div#menu2, p#menu, span.mainMenu {  display:none; } div#news { margin-top:10px; } a { text-decoration:underline; }</style>\n');
}
*/

function changeURL(url) {
  document.location.href=url;
}

var WindowObjectReference = null; // global variable
function openWin(strUrl, strWindowName) {
  if (WindowObjectReference == null || WindowObjectReference.closed) {
      WindowObjectReference = window.open(strUrl, strWindowName,"resizable=yes,scrollbars=yes,status=yes");
  } else {
      WindowObjectReference.focus();
      WindowObjectReference.location.href(strUrl);
  }
}

// Open link in new tabs/windows by Javascript, compliant with XHTML 1.0 Strict:
function externalLinks() {
  if (!document.getElementsByTagName) return;

  var anchors = document.getElementsByTagName("a"); // Array of all <a> tags on page.

  for (var i=0; i<anchors.length; i++) {
    var anchor = anchors[i];
    if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external") {
        // On page, must include rel="external" in the <a> tag.
        anchor.title = "Link opens in new window.";
        anchor.target = "_blank"; // Opens a new window for each clicked link.
        // anchor.target = "external";  // Reuses the same window, if it exists, else creates it.
                                        // NOTE: Might not bring existing window to focus when new
                                        // link is clicked, that is a system dependency.
    }
  }
}
window.onload = externalLinks;

// For the hover-to-appear sidebar nav for the SwiftX and SwiftForth sections.
function handleTOC() {

    // Change the DIV visibility state.
    if (document.getElementById('thisTOC').style.opacity < 1) {
        document.getElementById('thisTOC').style.opacity = 1;
        return false;
    }
    // For touchscreens: if the opacity is >= 1, let the page reload to restore
    // the DIV's original state.
}
