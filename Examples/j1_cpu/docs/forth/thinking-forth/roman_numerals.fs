\
\ Roman numerals
\ Implementation by Leo Brodie from "Thinking Forth" (online edition),
\ http://thinking-forth.sourceforge.net
\
\ Adapted for Gforth syntax 2012 by Dr. Wolfram Schroers,
\ http://www.field-theory.org
\

create romans      ( ones) char I c, char V c,
                   ( tens) char X c, char L c,
               ( hundreds) char C c, char D c,
              ( thousands) char M c,

variable column# ( current_offset)
: ones 0 column# ! ;
: tens 2 column# ! ;
: hundreds 4 column# ! ;
: thousands 6 column# ! ;

: column ( -- address-of-column) romans column# @ + ;

: .symbol ( offset -- ) column + c@ emit ;
: oner 0 .symbol ;
: fiver 1 .symbol ;
: tener 2 .symbol ;

: oners ( #-of-oners -- )
    ?dup if 0 do oner loop then ;
: almost ( quotient-of-5/ -- )
    oner if tener else fiver then ;
: digit ( digit -- )
    5 /mod over 4 =
    if almost drop
    else
	if fiver then
	oners then ;

: roman ( number -- )
    1000 /mod thousands digit
    100 /mod hundreds digit
    10 /mod tens digit
    ones digit ;
