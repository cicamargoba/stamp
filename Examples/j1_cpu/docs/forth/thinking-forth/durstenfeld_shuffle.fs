\
\ Durstenfeld shuffle
\ Durstenfeld implementation of Fisher-Yates shuffle in Gforth.
\ Requires 'random.fs' which exports the word 'random ( n -- 0..n-1)'.
\ See:
\ http://en.wikipedia.org/wiki/Fisher-Yates_shuffle
\
\ Main user interface words:
\ init-card-storage -- Sets up a standard sorted card order.
\ type-cards        -- Types the entire deck to stdout.
\ shuffle-cards     -- Shuffles (unbiased) the array of cards.
\
\ Written 2012 by Dr. Wolfram Schroers,
\ http://www.field-theory.org
\

s" random.fs" included

\ Representation of cards (standard 52-card Poker deck).
52 constant #cards
0  constant Clubs
1  constant Hearts
2  constant Spades
3  constant Diamonds
0  constant Ace
10 constant Jack
11 constant Queen
12 constant King

create card-storage #cards cells allot

\ Create and initialize storage of card array.
: init-cards ( -- )
    #cards 0 u+do
        i card-storage i cells + !
    loop ;

	
\ User-interface for handling the array of cards.
: suite-type ( n -- )
    case
        Clubs    of ." Clubs"    endof
        Hearts   of ." Hearts"   endof
        Spades   of ." Spades"   endof
        Diamonds of ." Diamonds" endof
    endcase ;

: value-type ( n -- )
    dup Ace   = if ." Ace "   else
    dup Jack  = if ." Jack "  else
    dup Queen = if ." Queen " else
    dup King  = if ." King "  else
    dup 1+ . then then then then drop ;

\ Type a single card with given number to stdout.
: card-type ( n -- )
    assert( dup 0 >= )
    assert( dup #cards < )
    13 /mod swap value-type ." of "
    suite-type ." ." cr ;

\ Type the current card-array to stdout.
: cards-type ( -- )
    #cards 0 u+do
	card-storage i cells + @ card-type
    loop ;


\ Shuffle the cards using the Durstenfeld shuffle.
\ @note The cards need to be initialized first.

\ Exchange cards at positions n1 and n2.
: exchange-cards ( n1 n2 -- )
    { n1 n2 }
    card-storage n1 cells + @
    card-storage n2 cells + @
    card-storage n1 cells + !
    card-storage n2 cells + ! ;

: shuffle-cards ( -- )
    #cards begin
        dup while
	    dup random swap 1- tuck exchange-cards
    repeat ;
