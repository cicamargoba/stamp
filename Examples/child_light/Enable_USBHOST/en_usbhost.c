/*******************************************************************************
* File Name          : en_usbhost.c
* Author             : Erwin Lopez
* Date First Issued  : 30/01/2013
* Description        : This file resets the USB selecting chip and switches to USB HOST.
********************************************************************************/

#include "board.h"
#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>


static int dev_mem_fd;
static void *sys_controller;
static unsigned int* pio_base;


int gpio_map(void){
	dev_mem_fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (dev_mem_fd < 0) {
          printf("Cannot open /dev/mem.\n");
	  return 0;
	}
	sys_controller = mmap(0, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, dev_mem_fd, IMX233_BASE_SYS);
	if (sys_controller == MAP_FAILED) {
          close(dev_mem_fd);
          printf("Cannot mmap.\n");
	  return 0;
	}
	pio_base = (unsigned int*)sys_controller;
        printf("GPIO maped successfully\n");
        return 0;
}

void init_gpio(void){

  struct device_t *device;
  device = devices;
  
  /*
   * Configure USB_SELECT, USB_RESET and LED GPIOs. 
   */

  pio_base[(2*( device->USBSEL_PIO ) + PIO_MUXSEL + 16*(device->USBSEL_PIN/16) + SET)/sizeof(unsigned int)] = ( 0x3 << 2*( (device->USBSEL_PIN - 16) ) );
  pio_base[(2*( device->LED_PIO )    + PIO_MUXSEL + 16*(device->LED_PIN/16)    + SET)/sizeof(unsigned int)] = ( 0x3 << 2*( device->LED_PIN ) );
  pio_base[(2*( device->USBRST_PIO ) + PIO_MUXSEL + 16*(device->USBRST_PIN/16) + SET)/sizeof(unsigned int)] = ( 0x3 << 2*( device->USBRST_PIN ) );
  
  /*
   * Configure REC, BT, USB, LED, BT as outputs. 
   */

  pio_base[(device->LED_PIO    + PIO_DOE + SET)/sizeof(unsigned int)] = device->USBSEL_MASK | device->LED_MASK;  // Set as OUTPUT
  pio_base[(device->USBRST_PIO + PIO_DOE + SET)/sizeof(unsigned int)] = device->USBRST_MASK;  // Set USBRST_MASK as OUTPUT
  printf("GPIO configured as outputs\n");

}

int main(void) {

  struct device_t *device;
  device = devices;

  gpio_map();
  init_gpio();

  printf("Begin USB Hub Reset.\n");
  pio_base[(device->USBRST_PIO + PIO_DOUT + CLR)/sizeof(unsigned int)] = device->USBRST_MASK;
  usleep (500 * 1000);
  pio_base[(device->USBRST_PIO + PIO_DOUT + SET)/sizeof(unsigned int)] = device->USBRST_MASK;
  usleep (500 * 1000);
  printf("Reset Done.\n");
  printf("USB Mode Change.\n");
  pio_base[(device->LED_PIO + PIO_DOUT + SET)/sizeof(unsigned int)] = device->LED_MASK;
  usleep (500 * 1000);
  pio_base[(device->LED_PIO + PIO_DOUT + CLR)/sizeof(unsigned int)] = device->LED_MASK;
  usleep (500 * 1000);
  pio_base[(device->USBSEL_PIO + PIO_DOUT + CLR)/sizeof(unsigned int)] = device->USBSEL_MASK;  
  usleep (500 * 1000);
  return 0;

}
